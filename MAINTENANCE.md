# Updating or Rerunning the Server for the App

This document provides instructions for updating or rerunning the server for our application hosted on a VPS (Virtual Private Server). Please follow these steps carefully to ensure the application is updated or restarted correctly.

## Prerequisites

Before proceeding, ensure you have the following:
- SSH client installed on your computer.
- Sufficient permissions to execute commands on the server.
- The password for switching to the root user if required.

## Connecting to the Server

1. Open your terminal or command prompt.
2. Use the following SSH command to connect to the server. You will be prompted to enter your password.

```bash
ssh youruser@hosting-server
```

## Switching to Root User

Once connected, you may need to switch to the root user to perform the following operations. Execute the following command and enter the password when prompted:

```bash
sudo su
```

## Navigating to the App Directory

Change to the application's directory using:

```bash
cd ../../opt/cartographie-ecosysteme-snds/
```

## Activating the Virtual Environment (Optional)

If your application requires a specific Python environment, activate it with:

```bash
. ../myenvs/carto39/bin/activate
```

This step is optional and depends on your application's setup.

## Updating the Application

Pull the latest updates from the Git repository:

```bash
git pull
```

## Rebuilding and Rerunning the App

1. Bring down the current Docker containers:

```bash
docker-compose down
```

2. Rebuild the Docker containers to incorporate any updates:

```bash
docker-compose build
```

3. Start the Docker containers in detached mode:

```bash
docker-compose up -d
```

## Verifying the Update

After the containers are up and running, verify that the application is functioning correctly. You may need to check the application logs or access the application via a web browser.

## Troubleshooting

If you encounter any issues during this process, review the application logs and ensure all steps were executed correctly. For specific errors, consulting the application's documentation or seeking help from a developer familiar with the technology stack might be necessary.