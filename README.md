# SNDS ecosystem cartography

Welcome to the git for the tool, hosted here : <https://ecosysteme-snds.health-data-hub.fr/>

> #### Disclaimer
> ⚠️ This WebApp is a work in progress! if you have any suggestions or find any Issues, please send an email at <opensource@health-data-hub.fr> or create an [Issue](https://gitlab.com/healthdatahub/cartographie-ecosysteme-snds/-/issues/new?issue%5Bmilestone_id%5D=)⚠️

## Presentation of the cartography
The main database of the French National Health Data System (SNDS) contains data from Health Insurance reimbursements, hospital treatment and medical causes of death. In order to characterise its use for health research and innovation, an interactive cartography has been produced to understand the framework of its use and to identify the stakeholders of the SNDS ecosystem. A bibliographic search via PubMed (available [here](lib/query.py)), [LiSSa](https://www.lissa.fr/dc/#env=lissa), [HAL](https://hal.archives-ouvertes.fr/) was conducted to identify scientific articles published starting January 2007 on studies using SNDS data. The list of authors, their affiliations, keywords, the number of citations and much more were collected. A descriptive analysis was carried out in order to assess temporal and geographical trends in the use of SNDS data.

The graphs where generated with [networkx](https://networkx.org/), a python package used for the creation manipulation and study of complex networks. To generate the Author/Affiliations graphs we first create the [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix) between the Authors/Affiliations and the article PMIDs. We then use [the networkx.Graph class](https://networkx.org/documentation/stable/reference/classes/graph.html#networkx.Graph) to create the needed undirected graphs, using the adjacency matrices as the data to intialize the graphs.

## Contribute

Using `conda install --file requirements.txt` or `pip install -r requirements.txt` will install all the dependencies.

Using `python dashboard.py` will launch the dahsboard locally in http://192.168.1.7:8050/

## Contributors and maintenance

This application and repository are maintained by the Health Data Hub. For any remarks or suggestions you can create an issue or contact the Health Data Hub on the email address <opensource@health-data-hub.fr>

## Licence

All content shared in this repository is published under the [Apache license](https://fr.wikipedia.org/wiki/Licence_Apache) version 2.0.
A copy of the license is available in the [LICENSE](LICENSE) file, or at http://www.apache.org/licenses/LICENSE-2.0.

### Explanations

The `Apache-2.0' license is a [free license](https://fr.wikipedia.org/wiki/Licence_libre).
This means that it gives anyone the freedom to use, study, modify and redistribute the content shared in this repository, _provided_ that the license and the authors of the content are stated.

### Citation
Please cite the cartography website if you use it in your work.

*tip Citation recommanded by APA*
> Cartographie interactive de l'écosystème SNDS. (n.d.). Retrieved from https://gitlab.com/healthdatahub/cartographie-ecosysteme-snds

### Note

When content produced in the framework of a public service mission is shared _without a licence_, the conditions for its re-use are set by the [Code des Relations entre le Public et l'Administration](https://www.legifrance.gouv.fr/affichCode.do?cidTexte=LEGITEXT000031366350).

A legal guide for the publication of free software in the administration is available on the Etalab mission website: [guide-juridique-logiciel-libre.etalab.gouv.fr/](https://guide-juridique-logiciel-libre.etalab.gouv.fr/).

## Acknowledgements
The HDH team would like to thank Pierre Alain Jachiet (HAS), Matthieu Doutreligne (HAS), Claire Imbaud (INSERM), and Julien Grosjean (CHU Rouen) for their contribution.
