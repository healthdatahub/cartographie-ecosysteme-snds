import base64
import glob
import os
from datetime import datetime
from typing import Any, Union

import pandas as pd

from app.utils import get_keywords, get_meshs, path_leaf
from lib import build, datareader

stamp = datetime.utcfromtimestamp(os.path.getatime("data/resources/all_DB.json"))

resource_data: dict[str, Union[pd.DataFrame, pd.Series]] = datareader.read_data()
graph_data: dict[str, datareader.ReadDataGraph] = datareader.read_graphs()

exploded_authors = resource_data["exploded_authors.json"]
authors = resource_data["backend authors.json"]
front_authors = resource_data["authors.json"]

frontend_publications = resource_data["publications.json"]
publications = resource_data["backend publications.json"]
publications_columns: list[dict[str, str]] = [
    {
        "name": i,
        "id": i,
        "presentation": "markdown",
    }
    for i in [
        "Titre",
        "Auteurs",
        "Journal",
        "Date de publication",
        "PMID",
        "doi",
        "Provenance",
        "Affiliations",
        "Type d'article",
    ]
]
ror_affiliations = resource_data["backend ror_affiliations.json"]
front_ror_affiliations = resource_data["ror_affiliations.json"]
ror_institutions_columns: list[dict[str, str]] = [
    {
        "name": i,
        "id": i,
        "presentation": "markdown",
    }
    for i in [
        "Structure",
        "PMID",
        "doi",
        "Auteurs",
        "Pays",
        "Ville",
    ]
]
ror_affiliations["city"] = ror_affiliations.addresses.apply(lambda x: x[0]["city"])
ror_affiliations["country_refactored"] = ror_affiliations.country.apply(lambda x: x["country_name"])
countries = sorted(list(set(ror_affiliations["country_refactored"])))
cities = sorted(list(set(ror_affiliations["city"])))

png_files_paths = glob.glob("./assets/*.png")
png_files_paths += glob.glob("./data/resources/*.png")
png_files = {}
for file_ in png_files_paths:
    png_files[path_leaf(file_)] = base64.b64encode(open(file_, "rb").read()).decode("ascii")
raw_meshs = resource_data[r"raw_mesh.json"]
meshs = resource_data[r"uniform_mesh_freqs.json"]

raw_keywords = resource_data[r"raw_keywords.json"]
keywords = resource_data[r"keyword_freqs.json"]
authors_names = list(authors["FullName"])

cyto_layout: dict[str, Any] = {
    "name": "cose-bilkent",
    "idealEdgeLength": 200,
    "nodeRepulsion": 10000,
    "animate": "end",
}
cyto_layout_inst: dict[str, Any] = {
    "name": "cose-bilkent",
    "idealEdgeLength": 100,
    "nodeRepulsion": 10000,
    "nodeDimensionsIncludeLabels": True,
    "animate": "end",
}
cyto_stylesheet: list[dict[str, Any]] = [
    {"selector": "edge", "style": {"line-color": "#DCDCDC"}},
    {"selector": "node", "style": {"content": "data(label)"}},
    {"selector": ".zero", "style": {"background-color": "#FFD60C"}},
    {"selector": ".one", "style": {"background-color": "#5DEEE3"}},
    {"selector": ".two", "style": {"background-color": "#65c5ae"}},
    {"selector": ".three", "style": {"background-color": "#abadff"}},
    {"selector": ".four", "style": {"background-color": "#a272ff"}},
    {"selector": ".five", "style": {"background-color": "#2570f1"}},
    {"selector": ".six", "style": {"background-color": "#036287"}},
    {"selector": ".seven", "style": {"background-color": "#06303a"}},
    {"selector": ".selected", "style": {"background-color": "red"}},
]
cyto_style: dict[str, Any] = {"width": "100%", "height": "500px"}

style_numbers = {
    "font-weight": "Bold",
    "color": "#612491",
    "font-size": "xx-large",
}
style_text = {
    "font-style": "italic",
    "color": "#06303A",
}
style_cards = []
for i in range(1, 6):
    style_cards.append(
        {
            "border": "0px",
            "backgroundColor": "#FFFFFF00",
            "background-image": f"url('data:image/png;base64,{png_files[f'fond-chiffre-0{i}.png']}')",
            "background-position": "middle left",
            "background-size": "100px 100px",
            "background-repeat": "no-repeat",
        }
    )
style_small_text = {
    "font-size": "xx-small",
    "color": "#612491",
}

SELECTED_AUTHOR = build.DEFAULT_AUTHOR
SELECTED_ROR = build.DEFAULT_STRUCTURE

dois = list(authors.loc[authors["FullName"] == SELECTED_AUTHOR].doi)

mdf = get_meshs(SELECTED_AUTHOR, raw_meshs, authors)
kdf = get_keywords(SELECTED_AUTHOR, raw_keywords, authors)

ppypj = pd.read_json(r"data/resources/plots/publications_per_year_per_journal.json", orient="records")

AUTHOR_TABLE_CLICK_TIMESTAMP = 0
STRUCTURE_TABLE_CLICK_TIMESTAMP = 0
