import datetime
from typing import Any, Union

import dash_bootstrap_components as dbc
import dash_cytoscape as cyto
import numpy as np
import pandas as pd
from dash import callback, dcc, html
from dash.dependencies import Input, Output, State
from networkx import Graph

import app.components as cmp
import app.constants as cst
import lib.markdown_elements as mds
from lib import network

pd.options.mode.chained_assignment = None  # default='warn'
cyto.load_extra_layouts()

ID = "authors"

page_authors_layout = dbc.Col(
    [
        html.Br(),
        cmp.updated_when_badge,
        html.H2("Auteurs d'articles travaillant avec le SNDS\t"),
        html.H5("Données et statistiques des auteurs publiant des articles sur le SNDS", style={"font-style": "italic"}),
        dbc.Card(
            [
                dbc.CardHeader("Trouver un Auteur"),
                dbc.CardBody(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    dbc.RadioItems(
                                        id=f"{ID}-and-or",
                                        options=[
                                            {"label": "AND", "value": 0},
                                            {"label": "OR", "value": 1},
                                        ],
                                        value=0,
                                    ),
                                    width={"size": "1"},
                                    align="end",
                                ),
                                dbc.Col(
                                    [
                                        html.Label(
                                            [
                                                "Filtrer par MeSH ",
                                                cmp.info_icon("filter-mesh-badge"),
                                                cmp.popover("filter-mesh-badge", mds.MDS["mesh_filter_text.md"]),
                                            ],
                                            style={"font-weight": "bold"},
                                        ),
                                        dcc.Dropdown(
                                            options=cst.raw_meshs.FullMeSH,
                                            id="MeSH-select-dropdown",
                                            multi=True,
                                            placeholder="Rechercher des MeSH",
                                        ),
                                    ]
                                ),
                                dbc.Col(
                                    [
                                        html.Label(
                                            [
                                                "Filtrer par Mots-Clefs ",
                                                cmp.info_icon("filter-keywords-badge"),
                                                cmp.popover("filter-keywords-badge", mds.MDS["keywords_filter_text.md"]),
                                            ],
                                            style={"font-weight": "bold"},
                                        ),
                                        dcc.Dropdown(
                                            options=cst.raw_keywords.NormalizedKeywords,
                                            id="Keywords-select-dropdown",
                                            multi=True,
                                            placeholder="Rechercher des Mots-Clefs",
                                        ),
                                    ]
                                ),
                            ]
                        ),
                        html.Hr(),
                        dbc.Row(
                            [
                                dbc.Col(
                                    dcc.Dropdown(
                                        options=cst.authors.FullName,
                                        id="author-select-dropdown",
                                        multi=False,
                                        placeholder="Rechercher un Auteur",
                                    ),
                                ),
                                dbc.Col(
                                    cmp.dropdown_table_download("Exporter ", ID, color="secondary"),
                                    width={"size": 2},
                                ),
                            ]
                        ),
                    ]
                ),
            ]
        ),
        html.Br(),
        dbc.Card(
            [
                dbc.CardHeader(
                    html.H4(
                        cst.SELECTED_AUTHOR,
                        id="authors-card-title",
                        style={"font-weight": "bold", "color": "#06303A"},
                    ),
                    style={
                        "background-color": "#5DEEE3",
                    },
                ),
                dbc.CardBody(
                    [
                        dbc.Row(
                            [
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="author-nb-publi", style=cst.style_numbers),
                                            html.H6("Publication(s) sur le SNDS dans Pubmed, LiSSa & HAL", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[0],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="author-sum-cit", style=cst.style_numbers),
                                            html.H6("Citations total", style=cst.style_text),
                                            html.Small(
                                                "*Total calculé sur les articles SNDS au moment de la récupération des données",
                                                style=cst.style_small_text,
                                            ),
                                        ],
                                        style=cst.style_cards[1],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="author-moy-coauthor", style=cst.style_numbers),
                                            html.H6("Nombre de co-auteurs moyen", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[2],
                                        body=True,
                                    )
                                ),
                                dbc.Col(
                                    dbc.Card(
                                        [
                                            html.H4(id="author-last-publi", style=cst.style_numbers),
                                            html.H6("Dernière publication sur le SNDS", style=cst.style_text),
                                        ],
                                        style=cst.style_cards[3],
                                        body=True,
                                    )
                                ),
                            ],
                        ),
                        html.A(id="auth-graph-links"),
                        dbc.Card(
                            [
                                dbc.CardHeader(
                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                html.H5(id="author-graph-card-hearder"),
                                                width={"size": 9},
                                            ),
                                            dbc.Col(
                                                cmp.download_button("authors-download-graph-button"),
                                                width={"order": "last"},
                                            ),
                                        ],
                                        justify="between",
                                    ),
                                ),
                                dbc.CardBody(
                                    [
                                        cyto.Cytoscape(
                                            id="author-graph",
                                            layout=cst.cyto_layout,
                                            style=cst.cyto_style,
                                            elements=cst.graph_data["authors"].sub_graph,
                                            autounselectify=True,
                                            stylesheet=cst.cyto_stylesheet,
                                            maxZoom=2,
                                            minZoom=0.3,
                                        ),
                                        html.Hr(),
                                        dbc.Row(
                                            [
                                                dbc.Row(
                                                    [
                                                        dbc.Col(
                                                            dbc.Button("Reset", id="author-graph-reset"),
                                                            width={"size": 1},
                                                        ),
                                                        dbc.Col(
                                                            [
                                                                dbc.Input(
                                                                    id="author-depth-limit",
                                                                    type="number",
                                                                    min=1,
                                                                    max=5,
                                                                    value=2,
                                                                ),
                                                                dbc.FormText("Profondeur"),
                                                            ],
                                                            width={"size": 2},
                                                        ),
                                                        dbc.Accordion(
                                                            dbc.AccordionItem(
                                                                cmp.generate_dashtable_element(
                                                                    id_="author-graph-tapnode-out",
                                                                    columns=[
                                                                        {
                                                                            "name": i,
                                                                            "id": i,
                                                                            "presentation": "markdown",
                                                                        }
                                                                        for i in ["Nom(s)", "PMID", "doi", "Affiliations"]
                                                                    ],
                                                                    data=cst.front_authors.loc[
                                                                        cst.front_authors["Nom(s)"].str.contains("Plancke L")
                                                                    ].to_dict(
                                                                        "records"
                                                                    ),  # type: ignore
                                                                    fixed_rows={"headers": False},
                                                                    style_table={
                                                                        "maxWidth": "100%",
                                                                        "overflowY": "scroll",
                                                                    },
                                                                ),
                                                                title="Info du Noeud Selectionné",
                                                            ),
                                                        ),
                                                    ]
                                                ),
                                            ],
                                        ),
                                    ],
                                ),
                            ]
                        ),
                        html.Br(),
                        html.A(id="auth-publications"),
                        dbc.Row(
                            dbc.Col(
                                [
                                    dbc.Card(
                                        [
                                            dbc.CardHeader(html.H5("Publications")),
                                            dbc.CardBody(
                                                cmp.generate_dashtable_element(
                                                    id_="author-publications-table",
                                                    columns=cst.publications_columns,
                                                    data=cst.frontend_publications.loc[
                                                        cst.frontend_publications.doi.isin(
                                                            cst.exploded_authors.loc[
                                                                cst.exploded_authors.FullName == cst.SELECTED_AUTHOR
                                                            ].doi.tolist()
                                                        )
                                                    ].to_dict(
                                                        "records"
                                                    ),  # type: ignore
                                                    style_table={"height": "auto", "maxWidth": "100%", "overflowY": "scroll"},
                                                    page_size=10,
                                                ),
                                            ),
                                        ],
                                    ),
                                ]
                            ),
                        ),
                        html.Br(),
                        html.A(id="auth-mesh-keywords"),
                        dbc.Row(
                            [
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardHeader(html.H5("Mots-Clés")),
                                                dbc.CardBody(
                                                    cmp.generate_dashtable_element(
                                                        id_="author-table-Keywords",
                                                        columns=[{"name": i, "id": i} for i in ["Mots-Clés", "Compte"]],
                                                        data=[],
                                                        style_table={},
                                                        style_cell={"font-family": "'Montserrat', sans-serif"},
                                                        fixed_rows={"headers": False},
                                                        page_size=17,
                                                    ),
                                                ),
                                            ],
                                        ),
                                    ]
                                ),
                                dbc.Col(
                                    [
                                        dbc.Card(
                                            [
                                                dbc.CardHeader(html.H5("MeSH (Medical Subject Headings)")),
                                                dbc.CardBody(
                                                    cmp.generate_dashtable_element(
                                                        id_="author-table-MeSH",
                                                        columns=[{"name": i, "id": i} for i in ["MeSH", "Compte"]],
                                                        data=[],
                                                        style_table={},
                                                        style_cell={"font-family": "'Montserrat', sans-serif"},
                                                        fixed_rows={"headers": False},
                                                        page_size=17,
                                                    ),
                                                ),
                                            ],
                                        ),
                                    ],
                                ),
                            ]
                        ),
                    ]
                ),
            ],
            color="#5DEEE3",
            outline=True,
            style={"background-color": "rgba(255,255,255, 0.9)"},
        ),
        html.Br(),
        html.Br(),
        cmp.end_page_accordion,
        html.P(id="inv_timestamp"),
    ],
    class_name="page-loader",
)
# Table
@callback(
    Output("author-select-dropdown", "options"),
    Input("MeSH-select-dropdown", "value"),
    Input("Keywords-select-dropdown", "value"),
    Input(f"{ID}-and-or", "value"),
)
def update_data_author(meshs: list[str], keywords: list[str], or_: bool) -> dict[str, Any]:
    """callback updates main author table

    Args:
        meshs (list[str]): mesh list from mesh dropdown menu item
        keywords (list[str]): keyword list from keyword dropdown menu item
        or_ (bool): if True will use AND, else OR

    Returns:
        list[dict[str, Any]]: filtered dataframe in to_dict("records") form
    """
    subset = cst.authors
    if meshs:
        grouped = cst.raw_meshs.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in meshs:
                ldois += grouped.doi[grouped.FullMeSH.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.FullMeSH.map(set(meshs).issubset)].tolist()
        names = cst.exploded_authors.loc[cst.exploded_authors.doi.isin(dois)].FullName
        subset = subset.loc[subset.FullName.isin(names)]
    if keywords:
        grouped = cst.raw_keywords.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in keywords:
                ldois += grouped.doi[grouped.NormalizedKeywords.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.NormalizedKeywords.map(set(keywords).issubset)].tolist()
        names = cst.exploded_authors.loc[cst.exploded_authors.doi.isin(dois)].FullName
        subset = subset.loc[subset.FullName.isin(names)]
    return subset.FullName


@callback(
    Output("author-publications-table", "data"),
    Input("author-select-dropdown", "value"),
)
def update_author_publications(name: str) -> Union[list[dict[str, Any]], dict[Any, Any]]:
    """callback updates mean coauthor of selected author

    Args:
        name (str): author name

    Returns:
        Union[list[dict[str, Any]], dict[Any, Any]]: publications of selected author
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    dois = cst.exploded_authors.loc[cst.exploded_authors.FullName == name].doi.tolist()
    return cst.frontend_publications.loc[cst.frontend_publications.doi.isin(dois)].to_dict("records")


# @callback(
#     Output("download-authors-json", "data"),
#     Input("menu-download-authors-json", "n_clicks"),
#     State("authors-table", "derived_virtual_indices"),
#     prevent_initial_call=True,
# )
# def download_authors_json(n_clicks: int, indices: list[int]):
#     """dowloads unique_authors.json

#     Args:
#         n_clicks (int): unused
#         indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

#     Returns:
#         dict of data frame content : dict of data frame content
#     """
#     return dcc.send_data_frame(cst.authors.iloc[indices, :].to_json, "unique_authors.json", indent=2)


# @callback(
#     Output("download-authors-csv", "data"),
#     Input("menu-download-authors-csv", "n_clicks"),
#     State("authors-table", "derived_virtual_indices"),
#     prevent_initial_call=True,
# )
# def download_authors_csv(n_clicks: int, indices: list[int]):
#     """dowloads unique_authors.csv

#     Args:
#         n_clicks (int): unused
#         indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

#     Returns:
#         dict of data frame content : dict of data frame content
#     """
#     return dcc.send_data_frame(cst.authors.iloc[indices, :].to_csv, "unique_authors.csv", sep=";")


# @callback(
#     Output("download-authors-xlsx", "data"),
#     Input("menu-download-authors-xlsx", "n_clicks"),
#     State("authors-table", "derived_virtual_indices"),
#     prevent_initial_call=True,
# )
# def download_authors_xlsx(n_clicks: int, indices: list[int]):
#     """dowloads unique_authors.xlsx

#     Args:
#         n_clicks (int): unused
#         indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

#     Returns:
#         dict of data frame content : dict of data frame content
#     """
#     return dcc.send_data_frame(cst.authors.iloc[indices, :].to_excel, "unique_authors.xlsx", index=False)


@callback(
    Output("authors-card-title", "children"),
    Input("author-select-dropdown", "value"),
)
def update_main_card_title(value: str) -> str:
    """updates main card title

    Args:
        name (str): bname of selected author

    Returns:
        str: the new author name
    """
    if not value:
        return cst.SELECTED_AUTHOR
    return value


# Stat Cards
@callback(
    Output("author-nb-publi", "children"),
    Input("author-select-dropdown", "value"),
)
def update_nb_publi_author(name: str) -> str:
    """callback updates number publications of selected author

    Args:
        name (str): author name

    Returns:
        str: number of publications
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    return f"{len(cst.authors.loc[cst.authors['FullName'] == name]['doi'].to_list()[0])}"


@callback(
    Output("author-moy-coauthor", "children"),
    Input("author-select-dropdown", "value"),
)
def update_moy_coauthor(name: str) -> str:
    """callback updates mean coauthor of selected author

    Args:
        name (str): author name

    Returns:
        str: mean coauthor of selected
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    return f"{int(np.mean(cst.publications[cst.publications.FullName.str.contains(name, regex=False)].FullName.str.len()-1))}"


@callback(
    Output("author-sum-cit", "children"),
    Input("author-select-dropdown", "value"),
)
def update_sum_cit_author(name: str) -> str:
    """callback updates total citations of selected author

    Args:
        name (str): author name

    Returns:
        name: name of selected author in dropdown
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    dois = cst.exploded_authors.loc[cst.exploded_authors.FullName == name].doi.tolist()
    return f"{int(sum(cst.publications.loc[cst.publications.doi.isin(dois)].PmcRefCount.dropna()))}*"


@callback(
    Output("author-last-publi", "children"),
    Input("author-select-dropdown", "value"),
)
def update_last_publi_author(name: str) -> str:
    """callback updates last publication date of selected author

    Args:
        name (str): author name

    Returns:
        str: last publication date of selected author
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    dois = cst.exploded_authors.loc[cst.exploded_authors.FullName == name].doi.tolist()
    list_publi_dates = cst.publications.loc[cst.publications.doi.isin(dois)].PubDate.dropna().to_list()
    list_publi_dates.sort()
    return f"{list_publi_dates[-1]}"


# MeSH & Keywords
@callback(
    Output("author-table-MeSH", "data"),
    Input("author-select-dropdown", "value"),
)
def update_mesh_table(name: str) -> Union[list[dict[str, Any]], dict[Any, Any]]:
    """updates mesh table of selected author

    Args:
        name (str): author name

    Returns:
        Union[list[dict[str, Any]], dict[Any, Any]]]: the mesh table
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    dois = cst.exploded_authors.loc[cst.exploded_authors.FullName == name].doi.tolist()
    mesh = (
        cst.raw_meshs.loc[cst.raw_meshs.doi.isin(dois)]
        .FullMeSH.value_counts()
        .to_frame()
        .reset_index()
        .rename(columns={"index": "MeSH", "FullMeSH": "Compte"})
    )
    return mesh.to_dict("records")


@callback(
    Output("author-table-Keywords", "data"),
    Input("author-select-dropdown", "value"),
)
def update_keyword_table(name: str) -> Union[list[dict[str, Any]], dict[Any, Any]]:
    """updates keyword table of selected author

    Args:
        name (str): author name

    Returns:
        Union[list[dict[str, Any]], dict[Any, Any]]: the keyword table

    """
    if not name:
        name = cst.SELECTED_AUTHOR
    dois = cst.exploded_authors.loc[cst.exploded_authors.FullName == name].doi.tolist()
    key = (
        cst.raw_keywords.loc[cst.raw_keywords.doi.isin(dois)]
        .NormalizedKeywords.value_counts()
        .to_frame()
        .reset_index()
        .rename(columns={"index": "Mots-Clés", "NormalizedKeywords": "Compte"})
    )
    return key.to_dict("records")


# Graph
@callback(
    Output("author-graph-card-hearder", "children"),
    Input("author-select-dropdown", "value"),
)
def update_graph_header_author(name: str) -> list[Any]:
    """callback updates title of author link graph title

    Args:
        name (str): author name

    Returns:
        str: title
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    return [
        f"Graphe des liens de co-autorat de {name} ",
        cmp.info_icon("author_graph_info_badge_id"),
        cmp.popover("author_graph_info_badge_id", mds.MDS["graphs.md"]),
    ]


@callback(
    Output("inv_timestamp", "children"),
    Input("author-select-dropdown", "value"),
)
def update_table_click_timestamp(_) -> None:
    """updates timestamp for table click

    Args:
        _ (unused): unused

    Returns:
        None: None
    """
    cst.AUTHOR_TABLE_CLICK_TIMESTAMP = int(datetime.datetime.now().timestamp() * 1000)


@callback(
    Output("author-graph", "elements"),
    Input("author-select-dropdown", "value"),
    Input("author-depth-limit", "value"),
    prevent_initial_call=True,
)
def update_graph_content_authors(name: str, depth: int) -> Graph:
    """callback updates link graph content

    Args:
        name (str): author name

    Returns:
        Graph: the graph item
    """
    if not name:
        name = cst.SELECTED_AUTHOR
    (assembled_graph, _, _) = network.sub_graph(cst.graph_data["authors"].full_graph, name, depth_limit=depth)
    return assembled_graph


@callback(
    Output("author-graph", "stylesheet"),
    Input("author-select-dropdown", "value"),
    Input("author-graph", "tapNode"),
    Input("author-graph-reset", "n_clicks_timestamp"),
    prevent_initial_call=True,
)
def authors_display_tap_node_data(_, node, n_clicks_timestamp):
    """callback displays the information of the selected node

    Args:
        node (dict): TapNode dict element info

    Returns:
        str: a string containing info on the tapped node
    """
    if not n_clicks_timestamp:
        n_clicks_timestamp = 0
    if node:
        timestamps = {
            "table": cst.AUTHOR_TABLE_CLICK_TIMESTAMP,
            "node": node["timeStamp"],
            "reset": n_clicks_timestamp,
        }
        choice = max(timestamps, key=timestamps.get)
        if choice in ["table", "reset"]:
            print("HERE")
            return cst.cyto_stylesheet
        else:
            stylesheet = [
                {
                    "selector": "node",
                    "style": {
                        "opacity": 0.3,
                        "label": "data(label)",
                        "text-opacity": 0.3,
                    },
                },
                {
                    "selector": "edge",
                    "style": {
                        "opacity": 0.2,
                        "curve-style": "bezier",
                    },
                },
                {
                    "selector": f'node[id = "{node["data"]["id"]}"]',
                    "style": {
                        "background-color": "#036287",
                        "border-color": "#2570f1",
                        "border-width": 1,
                        "border-opacity": 1,
                        "opacity": 1,
                        "label": "data(label)",
                        "color": "#036287",
                        "text-opacity": 1,
                        "font-size": 20,
                        "z-index": 9999,
                    },
                },
            ]

            for edge in node["edgesData"]:
                if edge["source"] == node["data"]["id"]:
                    stylesheet.append(
                        {
                            "selector": f'node[id = "{edge["target"]}"]',
                            "style": {
                                "background-color": "#a272ff",
                                "opacity": 0.9,
                                "content": "data(label)",
                                "text-opacity": 1,
                                "color": "#a272ff",
                            },
                        }
                    )
                    stylesheet.append(
                        {
                            "selector": f'edge[id= "{edge["id"]}"]',
                            "style": {
                                "mid-target-arrow-color": "#a272ff",
                                "mid-target-arrow-shape": "vee",
                                "line-color": "#a272ff",
                                "opacity": 0.9,
                                "z-index": 5000,
                            },
                        }
                    )

                if edge["target"] == node["data"]["id"]:
                    stylesheet.append(
                        {
                            "selector": f'node[id = "{edge["source"]}"]',
                            "style": {
                                "background-color": "#06303a",
                                "opacity": 0.9,
                                "z-index": 9999,
                                "content": "data(label)",
                                "text-opacity": 1,
                                "color": "#06303a",
                            },
                        }
                    )
                    stylesheet.append(
                        {
                            "selector": f'edge[id= "{edge["id"]}"]',
                            "style": {
                                "mid-target-arrow-color": "#06303a",
                                "mid-target-arrow-shape": "vee",
                                "line-color": "#06303a",
                                "opacity": 1,
                                "z-index": 5000,
                            },
                        }
                    )
            return stylesheet
    else:
        return cst.cyto_stylesheet


@callback(
    Output("author-graph", "generateImage"),
    Input("authors-download-graph-button", "n_clicks"),
    State("author-graph", "elements"),
    prevent_initial_call=True,
)
def unique_authors_get_image(_, elements: list) -> dict:
    """callback gets the image and preps for download

    Args:
        elements (list): list of the elements from cytograph to download

    Returns:
        dict: the download info
    """
    filename = f"Cyto_graph_source_{elements[0]['data']['label']}"
    return {
        "type": "svg",
        "action": "download",
        "filename": filename,
    }


@callback(
    Output("author-graph-tapnode-out", "data"),
    Input("author-graph", "tapNode"),
    prevent_initial_call=True,
)
def update_tapnode_data(node: str) -> Union[list[dict[str, Any]], dict[Any, Any]]:
    """updates main card title

    Returns:
        str: the new author name
    """
    return cst.front_authors.loc[cst.front_authors["Nom(s)"].str.contains(node["data"]["id"])].to_dict("records")
