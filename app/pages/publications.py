from typing import Any

import dash_bootstrap_components as dbc
import plotly.express as px
from dash import callback, dcc, html
from dash.dependencies import Input, Output, State

import app.components as cmp
import app.constants as cst
import lib.markdown_elements as mdelements
from lib import plots

ID = "publication"

page_publications_layout = dbc.Col(
    [
        html.Br(),
        cmp.updated_when_badge,
        html.H2("Publications sur le SNDS\t"),
        html.H5("Données et statistiques des publications sur le SNDS", style={"font-style": "italic"}),
        dbc.Card(
            [
                dbc.Row(
                    [
                        dbc.Col(
                            dbc.RadioItems(
                                id=f"{ID}-and-or",
                                options=[
                                    {"label": "AND", "value": 0},
                                    {"label": "OR", "value": 1},
                                ],
                                value=0,
                            ),
                            width={"size": "1"},
                            align="end",
                        ),
                        dbc.Col(
                            [
                                html.Label(
                                    [
                                        "Filtrer par MeSH ",
                                        cmp.info_icon(f"{ID}-filter-mesh-badge"),
                                        cmp.popover(f"{ID}-filter-mesh-badge", mdelements.MDS["mesh_filter_text.md"]),
                                    ],
                                    style={"font-weight": "bold"},
                                ),
                                dcc.Dropdown(
                                    options=cst.meshs["FullMeSH"],
                                    id=f"{ID}-MeSH-select-dropdown",
                                    multi=True,
                                    placeholder="Rechercher des MeSH",
                                ),
                            ]
                        ),
                        dbc.Col(
                            [
                                html.Label(
                                    [
                                        "Filtrer par Mots-Clefs ",
                                        cmp.info_icon(f"{ID}-filter-keywords-badge"),
                                        cmp.popover(f"{ID}-filter-keywords-badge", mdelements.MDS["keywords_filter_text.md"]),
                                    ],
                                    style={"font-weight": "bold"},
                                ),
                                dcc.Dropdown(
                                    options=cst.keywords["NormalizedKeywords"],
                                    id=f"{ID}-Keywords-select-dropdown",
                                    multi=True,
                                    placeholder="Rechercher des Mots-Clefs",
                                ),
                            ]
                        ),
                        dbc.Col(
                            cmp.dropdown_table_download("Exporter ", ID, color="secondary"),
                            width={"size": 2, "order": 5},
                            align="end",
                        ),
                    ]
                ),
                html.Br(),
                cmp.generate_dashtable_element(
                    id_="publications-table",
                    columns=cst.publications_columns,
                    data=cst.frontend_publications.to_dict("records"),
                    style_table={"height": "600px", "maxWidth": "100%", "overflowY": "scroll"},
                    page_size=10,
                ),
                html.Br(),
                html.A(id="pub-graphs"),
                cmp.card_generate_full_plots(title=["Publications par année"], id_="publications_per_year"),
                html.Br(),
                cmp.card_generate_full_plots(
                    title=[
                        "Publications par journal\t",
                        cmp.info_icon("publications_per_journal_info_badge_id"),
                        cmp.popover("publications_per_journal_info_badge_id", mdelements.MDS["publications_journals.md"]),
                    ],
                    id_="publications_per_journal",
                ),
                html.Br(),
                dbc.Card(
                    [
                        dbc.CardHeader(html.H5("Publications par annnée par journal")),
                        dbc.CardBody(
                            [
                                dbc.Row(
                                    [
                                        dbc.Col(
                                            dbc.Button(
                                                "Plot",
                                                id=f"plot-{ID}-journals-per-year",
                                                color="secondary",
                                            ),
                                            width={"size": 1},
                                        ),
                                        dbc.Col(
                                            dcc.Dropdown(
                                                options=cst.ppypj.sort_values(by=["CumSum"], ascending=False)["Journal-Title"].unique(),
                                                id=f"{ID}-journals-select-dropdown",
                                                multi=True,
                                                placeholder="Selectionner des Journaux",
                                            ),
                                            width={"size": 9},
                                        ),
                                        dbc.Col(
                                            [
                                                dbc.Checklist(
                                                    options=[
                                                        {"label": "cumulatif", "value": 1},
                                                    ],
                                                    value=[1],
                                                    id=f"{ID}-journals-cumulatif-radio",
                                                ),
                                            ]
                                        ),
                                    ],
                                ),
                                html.Br(),
                                dbc.Row(dcc.Graph(id=f"{ID}-journals-per-year")),
                            ],
                        ),
                    ]
                ),
                html.Br(),
                dbc.Row(
                    [
                        cmp.col_generate_half_plot(title=["Langue des Publications"], id_=f"{ID}-lang"),
                        cmp.col_generate_half_plot(title=["Provenance des Publications"], id_="publications-db-pie"),
                    ]
                ),
                html.Br(),
                cmp.card_generate_full_plots(title=["Langue des Publications par année"], id_=f"{ID}-lang_per-year"),
                html.Br(),
                html.A(id="pub-mesh-keywords"),
                dbc.Card(
                    [
                        dbc.CardHeader([html.H5("Toutes les MeSHs des Publications\t")]),
                        dbc.CardBody(
                            [
                                dbc.Row(
                                    [
                                        cmp.col_uniformmesh_cloud,
                                        cmp.col_uniformmesh_table,
                                    ],
                                ),
                            ],
                        ),
                    ],
                ),
                html.Br(),
                dbc.Card(
                    [
                        dbc.CardHeader([html.H5("MeSHs Sujet Majeur (*) des Publications\t")]),
                        dbc.CardBody(
                            [
                                dbc.Row(
                                    [
                                        cmp.col_mainmesh_cloud,
                                        cmp.col_mainmesh_table,
                                    ],
                                ),
                            ],
                        ),
                    ],
                ),
                html.Br(),
                dbc.Card(
                    [
                        dbc.CardHeader(
                            [
                                html.H5(
                                    [
                                        "Mots-clés des Publications\t",
                                        cmp.info_icon("keywords_info_badge_id"),
                                        cmp.popover("keywords_info_badge_id", mdelements.MDS["keywords.md"]),
                                    ],
                                )
                            ]
                        ),
                        dbc.CardBody(
                            [
                                dbc.Row(
                                    [
                                        cmp.col_keyword_cloud,
                                        cmp.col_keyword_table,
                                    ],
                                ),
                            ],
                        ),
                    ],
                ),
            ],
            outline=True,
            body=True,
        ),
        html.Br(),
        html.Br(),
        cmp.end_page_accordion,
    ],
    class_name="page-loader",
)


@callback(
    Output(f"{ID}-journals-per-year", "figure"),
    Input(f"plot-{ID}-journals-per-year", "n_clicks"),
    State(f"{ID}-journals-select-dropdown", "value"),
    State(f"{ID}-journals-cumulatif-radio", "value"),
)
def plot_pubs_per_year_per_journal(_, journals: list[str], cumsum: bool):
    """plots publications per year per journal

    Args:
        _ (unused): unused
        journals (list[str]): list of Journal-Title
        cumsum (bool): if cumsum or not

    Returns:
        px.line: the publications_per_year_per_journal plot
    """
    if journals:
        data = cst.ppypj.loc[cst.ppypj["Journal-Title"].isin(journals)]
        return plots.plot_publications_per_year_per_journal(data, cumsum)
    else:
        fig = px.line()
        fig.update_layout(
            xaxis={"visible": False},
            yaxis={"visible": False},
            plot_bgcolor=plots.BG_COLOR,
            annotations=[
                {
                    "text": "Sélectionner un ou plusieurs journaux",
                    "xref": "paper",
                    "yref": "paper",
                    "showarrow": False,
                    "font": {"size": 28},
                }
            ],
        )
        return fig


@callback(
    Output(f"{ID}-lang_per-year", "figure"),
    Input("publications-table", "data"),
)
def plot_publications_per_lang_per_year(_):
    """callback creates and returns plot_publications_per_lang_per_year

    Returns:
        tuple: px.line
    """
    return plots.plot_publications_per_lang_per_year()


@callback(
    Output("publications-db-pie", "figure"),
    Input("publications-table", "data"),
)
def plot_pie_article_db(_):
    """callback creates and returns particle db pie

    Returns:
        tuple: a tuple with 2 plots, city per author and country per author
    """
    return plots.plot_publications_per_db_pie(cst.publications)


# Publications Callbacks
@callback(
    Output("publications-table", "data"),
    Input(f"{ID}-MeSH-select-dropdown", "value"),
    Input(f"{ID}-Keywords-select-dropdown", "value"),
    Input(f"{ID}-and-or", "value"),
)
def update_data_publications(meshs: list[str], keywords: list[str], or_: bool) -> dict[str, Any]:
    """callback updates main publications table

    Args:
        meshs (list[str]): mesh list from mesh dropdown menu item
        keywords (list[str]): keyword list from keyword dropdown menu item
        or_ (bool): if True will use AND, else OR

    Returns:
        list[dict[str, Any]]: filtered dataframe in to_dict("records") form
    """
    if meshs:
        grouped = cst.raw_meshs.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in meshs:
                ldois += grouped.doi[grouped.FullMeSH.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.FullMeSH.map(set(meshs).issubset)].tolist()
        return cst.frontend_publications.loc[cst.frontend_publications.doi.isin(dois)].to_dict("records")  # type: ignore
    if keywords:
        grouped = cst.raw_keywords.groupby("doi").agg(list).reset_index()
        if or_:
            ldois = []
            for ele in keywords:
                ldois += grouped.doi[grouped.NormalizedKeywords.map(lambda x: ele in x)].tolist()
            dois = set(ldois)
        else:
            dois = grouped.doi[grouped.NormalizedKeywords.map(set(keywords).issubset)].tolist()
        return cst.frontend_publications.loc[cst.frontend_publications.doi.isin(dois)].to_dict("records")  # type: ignore
    return cst.frontend_publications.to_dict("records")  # type: ignore


@callback(
    Output("publications_per_year", "figure"),
    Output("publications_per_journal", "figure"),
    Input("publications-table", "data"),
)
def publications_display_plot(_) -> tuple:
    """callback creates and returns publication plots

    Returns:
        tuple: a tuple with 2 plots, Publications per year and SNDS Publications per Journal
    """
    return plots.plot_publications_per_year(), plots.plot_publications_per_journal()


@callback(
    Output(f"{ID}-lang", "figure"),
    Input("publications-table", "data"),
)
def publications_lang_plot(_) -> tuple:
    """loads lang publication plot

    Returns:
        tuple: a tuple with 2 plots, Publications per year and SNDS Publications per Journal
    """
    return plots.plot_lang_pie()


@callback(
    Output(f"download-{ID}-json", "data"),
    Input(f"menu-download-{ID}-json", "n_clicks"),
    State("publications-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_publications_json(_, indices: list[int]):
    """dowloads publications.json

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    data = cst.publications.sort_values(by="PubDate", ascending=False).iloc[indices, :].to_json
    return dcc.send_data_frame(data, "publications.json", indent=2)


@callback(
    Output(f"download-{ID}-csv", "data"),
    Input(f"menu-download-{ID}-csv", "n_clicks"),
    State("publications-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_publications_csv(_, indices: list[int]):
    """dowloads publications.csv

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    data = cst.publications.sort_values(by="PubDate", ascending=False).iloc[indices, :].to_csv
    return dcc.send_data_frame(data, "publications.csv", sep=";")


@callback(
    Output(f"download-{ID}-xlsx", "data"),
    Input(f"menu-download-{ID}-xlsx", "n_clicks"),
    State("publications-table", "derived_virtual_indices"),
    prevent_initial_call=True,
)
def download_publications_xlsx(_, indices: list[int]):
    """dowloads publications.xlsx

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    data = cst.publications.sort_values(by="PubDate", ascending=False).iloc[indices, :].to_excel
    return dcc.send_data_frame(data, "publications.xlsx", index=False)
