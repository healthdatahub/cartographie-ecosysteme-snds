import ntpath
from typing import Union

import pandas as pd


def path_leaf(path: str) -> str:
    """gets filename in path

    Args:
        path (str): the path

    Returns:
        str: filename
    """
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def get_meshs(
    author: str,
    raw_meshs: Union[pd.DataFrame, pd.Series],
    unique_authors: Union[pd.DataFrame, pd.Series],
) -> Union[pd.DataFrame, pd.Series]:
    """gets mesh of specific author

    Args:
        author (str): name of author
        raw_meshs (Union[pd.DataFrame, pd.Series]): resource_data[r"raw_mesh.json"]
        unique_authors (Union[pd.DataFrame, pd.Series]): resource_data[r"backend unique_authors.json"]


    Returns:
        Union[pd.DataFrame, pd.Series]: meshs of specific author
    """
    dois = list(unique_authors.loc[unique_authors["FullName"] == author].doi)
    mcount = raw_meshs.loc[raw_meshs["doi"].isin(dois[0])].FullMeSH.value_counts()
    mdf = pd.DataFrame()
    mdf["MeSH"] = mcount.keys()
    mdf["Count"] = mcount.values
    return mdf


def get_keywords(
    author: str,
    raw_keywords: Union[pd.DataFrame, pd.Series],
    unique_authors: Union[pd.DataFrame, pd.Series],
) -> Union[pd.DataFrame, pd.Series]:
    """gets keyword of specific author

    Args:
        author (str): name of author
        raw_meshs (Union[pd.DataFrame, pd.Series]): resource_data[r"raw_keywords.json"]
        unique_authors (Union[pd.DataFrame, pd.Series]): resource_data[r"backend unique_authors.json"]

    Returns:
        Union[pd.DataFrame, pd.Series]: keywords of specific author
    """
    dois = list(unique_authors.loc[unique_authors["FullName"] == author].doi)
    kcount = raw_keywords.loc[raw_keywords["doi"].isin(dois[0])].RawKeywords.value_counts()
    kdf = pd.DataFrame()
    kdf["Keywords"] = kcount.keys()
    kdf["Count"] = kcount.values
    return kdf
