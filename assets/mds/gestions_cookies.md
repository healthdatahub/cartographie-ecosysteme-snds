## Définition

Un "Cookie" est une suite d'informations, généralement de petite taille et identifié par un nom, qui peut être transmis à votre navigateur par un site web sur lequel vous vous connectez. Les Cookies ont de multiples usages, ils peuvent notamment servir à mémoriser votre identifiant client auprès d'un site marchand, le contenu de votre panier d'achat, ou encore tracer votre navigation pour des finalités statistiques ou publicitaires.

Lors de sa visite sur la cartographie interactive de l’écosystème SNDS, le Visiteur peut consentir ou refuser au dépôt de Cookies de mesure d’audience sur son équipement terminal. Veuillez noter que l’installation d’un Cookie nécessite un acte volontaire de votre part.


Le Visiteur donne son consentement de façon indépendante et spécifique pour la finalité détaillée ci-dessous et a la possibilité de retirer son consentement à tout moment. Votre navigateur web le conservera pendant une durée de 13 mois maximum et le renverra au serveur web chaque fois que vous vous y connecterez.

Ces réglages sont valables uniquement sur l’équipement et le navigateur que vous utilisez au moment où vous exprimez votre choix.

Vous pouvez également vous référer au bandeau d’information des Cookies qui apparaît, tant que vous n’avez pas exprimé votre choix, sur notre site internet.

## Finalités des Cookies

Sur le site de la cartographie interactive de l’écosystème SNDS géré par le Health Data Hub, des cookies sont susceptibles d’être installés afin de réaliser des mesures d’audience. Aucun Cookie tiers utilisé par les réseaux sociaux (Facebook, Google, Twitter, etc.) n’est susceptible d’être déposé sur votre terminal.
Précisément, les Cookies utilisées pour la mesure d’audience relèvent de la solution Matomo Analytics. Les informations de navigation stockées sont le nombre de visites par jour, les données de connexion, le moteur de recherche utilisé, le temps passé sur la page, le flux des pages visitées, le temps de chargement, le pays de l’utilisateur, l’heure de la visite, le journal des visites, la page d’entrée, la page de sortie, les liens sortants et les téléchargements. En cas de refus de ces Cookies, les mesures d'audience ne seront pas relevées.

## Durée de conservation
La durée de vie de ces Cookies est de 13 mois maximum.

## Suppression des Cookies

Vous pouvez paramétrer votre navigateur pour accepter ou refuser les Cookies de mesure d’audience préalablement à leur installation.

Vous pouvez également régulièrement supprimer les Cookies de votre terminal via votre navigateur.

Pour la gestion des Cookies et de vos choix, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d’aide de votre navigateur, qui vous permettra de savoir de quelle manière modifier vos souhaits en matière de Cookies.

A titre d’exemple :

1. Pour Internet Explorer : <http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies> ;
2. Pour Safari : <http://docs.info.apple.com/article.html?path=Safari/3.0/fr/9277.html> ;
3. Pour Chrome : <http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647> ;
4. Pour Firefox : <http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies> ;

Ces réglages sont valables uniquement sur l’équipement et le navigateur que vous utilisez au moment où vous exprimez votre choix.

## Mise à jour

La présente Politique de confidentialité peut être amenée à évoluer selon le contexte légal et réglementaire ainsi que de la doctrine de la CNIL. Le Health Data Hub invite le Visiteur à consulter régulièrement la présente page pour prendre connaissance des changements et des nouvelles informations.
