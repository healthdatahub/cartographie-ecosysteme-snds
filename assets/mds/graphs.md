Les graphes sont créés avec l'outil [networkx](https://networkx.org/), un package python utilisé pour la création, la manipulation et l'étude de réseaux complexes. Pour générer les différents graphes nous créons la [matrice d'adjacence](https://fr.wikipedia.org/wiki/Matrice_d%27adjacence) entre les Auteurs/Affiliations et les PMIDs des articles.
Nous utilisons ensuite l'objet [networkx.Graph](https://networkx.org/documentation/stable/reference/classes/graph.html#networkx.Graph) afin de créer les graphs non dirigés en utilisant la matrice d'adjacence comme données d'initialisation.

Une recherche [depth-first-search](https://fr.wikipedia.org/wiki/Algorithme_de_parcours_en_profondeur) de l'auteur et une conversion s'effectue entre le Graph complet format networkx a un format compatible avec [Dash Cytoscape](https://dash.plotly.com/cytoscape)

