​Le Health Data Hub s’engage, dans le cadre de ses activités et conformément à la législation en vigueur en France et en Europe, à assurer la protection, la confidentialité et la sécurité des données à caractère personnel des Visiteurs de son site ainsi qu’à respecter leur vie privée.

## Gestion des Données à caractère personnel

### Définition

La présente Politique de confidentialité vous informe sur la façon dont la plateforme des données de santé ou « Health Data Hub » traite vos données personnelles via le site internet de la cartographie de l’écosystème SNDS.
Le site de la cartographie de l’écosystème SNDS (disponible au lien suivant <https://ecosysteme-snds.health-data-hub.fr/> a pour vocation de mettre à la disposition de la communauté des utilisateurs et personnes intéressées par le Système National des Données de Santé ( « SNDS » ) une cartographie interactive sur les utilisateurs et l’écosystème utilisant le SNDS. Cette cartographie interactive est construite par le HDH à partir d’informations publiques et issues de PubMed (<https://pubmed.ncbi.nlm.nih.gov/>). Tout internaute connecté à ce site est réputé avoir pris connaissance des mentions légales et conditions d’utilisation ci-après.

Cette Politique est susceptible d’évoluer en fonction du contexte légal et réglementaire ainsi que de la doctrine de la CNIL.

Les termes ci-dessous listés ont la définition suivante :

Visiteur : désigne toute personne visitant le site internet de la la cartographie interactive de l’écosystème SNDS ;

Site internet de la cartographie interactive de l’écosystème SNDS : désigne le site internet ayant pour vocation de mettre à la disposition de la communauté des utilisateurs et personnes intéressées par le SNDS une cartographie interactive de la communauté et utilisateur du SNDS. Ce site est accessible à l’adresse suivante <https://ecosysteme-snds.health-data-hub.fr/>;

Délégué à la protection des données : désigne la personne en charge de conseiller et contrôler le Responsable de traitement en matière de protection des données personnelles ;

Responsable de traitement : est la personne morale incarnée par son représentant légal qui détermine les finalités et les moyens d’un traitement. Ici le Health Data Hub est responsable des traitements de données à caractère personnel mis en oeuvre sur la cartographie interactive de l’écosystème SNDS ;
Données à caractère personnel : désignent toute information relative à une personne physique directement ou indirectement identifiable ;

Commission Nationale de l’Informatique et des Libertés (CNIL) : désigne l’autorité de contrôle en charge du respect des obligations en matière de données à caractère personnel en France.
Règlement Général sur la Protection des Données (RGPD) : texte réglementaire européen qui encadre le traitement des données à caractère personnel sur tout le territoire de l’Union européenne (n°2016/679 du 27 avril 2016).


## Finalité et base légale des traitements de Données à caractère personnel

Le traitement de vos données à caractère personnel est soumis au respect du RGPD et à la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

Le Health Data Hub agit en tant que Responsable de traitement au sens du RGPD.

Sur la base de leur consentement (article 6.1.a du RGPD), les Visiteurs peuvent entrer en contact avec le Health Data Hub en cas de remarques, demande d’informations, suggestions ou pour remonter un problème survenu sur ce site internet, en envoyant un mail à l’adresse indiquée sur le site (opensource@health-data-hub.fr). A cette occasion, les Visiteurs transmettent leur adresse email et éventuellement toute autre information à caractère personnel utile pour traiter la demande.

Le Health Data Hub veille à ne collecter et ne traiter que des données strictement nécessaires au regard de cette finalité mentionnée. Le Health Data Hub s’engage à respecter la confidentialité des messages qui lui sont transmis.


## Destinataires des Données à caractère personnel

Vos informations personnelles sont uniquement traitées par les personnes habilitées du Health Data Hub et ne sont en aucun cas cédées à des tiers ou utilisées pour d’autres finalités.

Les données peuvent être transmises aux autorités compétentes, à leur demande, dans le cadre de procédures judiciaires, dans le cadre de recherches judiciaires et de sollicitations d’information à la demande des autorités ou afin de se conformer à d’autres obligations légales.

## Durée de conservation des Données à caractère personnel

Vos données sont conservées jusqu’à la durée nécessaire à l’accomplissement du traitement de votre demande. A l’issue de cette période, vos données seront supprimées.

## Sécurité des Données à caractère personnel
Le Health Data Hub assure la sécurité et la confidentialité de vos données à caractère personnel en mettant en place une protection des données renforcée par l’utilisation de moyens de sécurisation physiques et logiques. Ces mesures techniques et organisationnelles visent à éviter la perte, la mauvaise utilisation, l’altération et la suppression des données à caractère personnel vous concernant. Ces mesures sont adaptées selon le niveau de sensibilité des données traitées et selon le niveau de risque que présente le traitement ou sa mise en œuvre.


## Vos droits

Conformément au RGPD et à la Loi Informatique et Libertés, vous disposez de droits au regard des données à caractère personnel que vous nous communiquez :

le droit d’accès vous permet de savoir si des données vous concernant sont traitées et d’en obtenir la communication ;

le droit de rectification vous permet de corriger des données inexactes vous concernant ou de compléter des données vous concernant ;

le droit à l’effacement de vos données ;

le droit à la portabilité des données, afin de récupérer les données vous concernant ;

le droit à la limitation du traitement, afin de geler l’utilisation des données vous concernant dans certains cas.

Vous disposez également du droit de retirer votre consentement à tout moment.

Vous pouvez consulter le site de la CNIL afin de bénéficier de plus d’informations concernant vos droits relatifs aux données à caractère personnel : <https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles> .

Afin d’obtenir des informations complémentaires sur le traitement de vos données ou exercer vos droits, vous pouvez contacter le Délégué à la protection des données du Health Data Hub par voie électronique via ce formulaire de contact en ligne, ou à l’adresse dpd@health-data-hub.fr, ou par voie postale au 9 rue Georges Pitard, 75015 Paris.
Une réponse sera adressée dans un délai d’un mois à compter de la réception de la demande.

Dans l’hypothèse où vous ne seriez pas satisfaits de nos échanges, vous pouvez adresser une réclamation auprès de la CNIL : <https://www.cnil.fr/fr/plaintes> .

## Transfert de données à caractère personnel hors de l’Union Européenne

Aucune donnée collectée n’est susceptible d’être transférée hors de l’Union Européenne.
