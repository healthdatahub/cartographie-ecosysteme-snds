
## Comment Utiliser ce Dashboard?
Il est divisé en 3 pages:

- ### [Publications](https://ecosysteme-snds.health-data-hub.fr/publications)
 
Cette page possède divers éléments sur les Publications sur le SNDS.  
Un tableau intéractif permet de visualiser tous les articles récupérés sur PubMed, LiSSa et HAL. Ce tableau est la base de données principale qui regroupe toutes les infos du dashboard.
 
- ### [Auteurs](https://ecosysteme-snds.health-data-hub.fr/auteurs)
 
Cette page est divisée en 2 parties avec la première permettant la recherche d'un auteur spécifique.  
Une fois l'auteur sélectionné, la deuxième partie de la page se met a jour automatiquement, révélant divers chiffres clés, le graphe de co-autorat et les publications de celui-ci
 
- ### [Structures](https://ecosysteme-snds.health-data-hub.fr/structures)
 
Similairement à la page des Auteurs, cette page est divisée en deux.  
La première permet de faire une recherche d'une structure spécifique avec quelques filtres (MeSH, Mots-clés, Ville & Pays) et la deuxième de visualiser les données et le graphe de co-autorat de la structure sélectionnée.
 

