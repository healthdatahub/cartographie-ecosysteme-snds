"""this is the main dashboard app"""
import dash_bootstrap_components as dbc
import dash_cytoscape as cyto
import pandas as pd
from dash import dash, dcc, html
from dash.dependencies import Input, Output

import app.components as cmp
import app.constants as cst
import app.pages.authors as ap
import app.pages.institutions as ip
import app.pages.publications as pp
import lib.markdown_elements as mds

# For Bootstrap Icons...
app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP, dbc.icons.BOOTSTRAP])
pd.options.mode.chained_assignment = None  # default='warn'

cyto.load_extra_layouts()

LIGHT_THEME = dbc.themes.LUMEN
ANNEXE_THEME = dbc.themes.ZEPHYR
DARK_THEME = dbc.themes.SLATE


# define app
app = dash.Dash(
    __name__,
    external_stylesheets=[ANNEXE_THEME, "https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css"],
)

app.title = "Ecosysteme SNDS"

sub_navlink_style: dict[str, str] = {"padding-left": "30px", "font-size": "0.9rem"}

navlink_closed_intro: dbc.NavLink = dbc.NavLink("Écosystème SNDS ", href="/", active="exact")
navlink_landing = [
    navlink_closed_intro,
    dbc.NavLink("Requêtes", href="#query", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Guide d'utilisation", href="#tutorial", style=sub_navlink_style, external_link=True),
]

navlink_closed_publications: dbc.NavLink = dbc.NavLink(
    [" Publications ", html.I(className="bi bi-journals")],
    href="/publications",
    active="exact",
)
navlink_publications = [
    navlink_closed_publications,
    dbc.NavLink("Recherche", href="#", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Graphiques", href="#pub-graphs", style=sub_navlink_style, external_link=True),
    dbc.NavLink("MeSHs & Mots-clefs", href="#pub-mesh-keywords", style=sub_navlink_style, external_link=True),
]

navlink_closed_authors: dbc.NavLink = dbc.NavLink(
    [" Auteurs ", html.I(className="bi bi-people-fill")],
    href="/auteurs",
    active="exact",
)
navlink_authors = [
    navlink_closed_authors,
    dbc.NavLink("Recherche", href="#", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Graphe des liens", href="#auth-graph-links", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Publications", href="#auth-publications", style=sub_navlink_style, external_link=True),
    dbc.NavLink("MeSHs & Mots-clefs", href="#auth-mesh-keywords", style=sub_navlink_style, external_link=True),
]

navlink_closed_structures: dbc.NavLink = dbc.NavLink(
    [" Structures ", html.I(className="bi bi-building")],
    href="/structures",
    active="exact",
)
navlink_structures = [
    navlink_closed_structures,
    dbc.NavLink("Graphiques", href="#", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Recherche", href="#struct-find", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Graphe des liens", href="#struct-graph-links", style=sub_navlink_style, external_link=True),
    dbc.NavLink("Publications", href="#struct-publications", style=sub_navlink_style, external_link=True),
    dbc.NavLink("MeSHs & Mots-clefs", href="#struct-mesh-keywords", style=sub_navlink_style, external_link=True),
]

app_index: dbc.Col = dbc.Col(
    [
        html.Br(),
        cmp.logo,
        dbc.Nav(
            [
                html.Br(),
                html.Div(id="landing"),
                html.Div(id="navbar-publications"),
                html.Div(id="navbar-authors"),
                html.Div(id="navbar-structures"),
                html.Br(),
                dbc.NavItem(html.H5("Données & Code")),
                dbc.DropdownMenu(
                    label="Exporter ",
                    group=True,
                    children=[
                        cmp.dropdown_table_download(label="Publications ", id_="Publications"),
                        cmp.dropdown_table_download(label="Authors ", id_="Authors"),
                        cmp.dropdown_table_download(label="Structures ", id_="Structures"),
                    ],
                    color="secondary",
                    nav=True,
                    in_navbar=True,
                    style={"text-transform": "capitalize"},
                ),
                dbc.NavLink(
                    [
                        "Code Source ",
                        html.I(className="bi bi-git"),
                    ],
                    href="https://gitlab.com/healthdatahub/cartographie-ecosysteme-snds",
                    active="exact",
                    target="_blank",
                ),
                html.Br(),
                dbc.NavItem(html.H5("Liens externes")),
                dbc.NavLink(
                    ["Documentation SNDS ", html.I(className="bi bi-box-arrow-up-right")],
                    href="https://documentation-snds.health-data-hub.fr/",
                    target="_blank",
                ),
                dbc.NavLink(
                    ["Structure SNDS ", html.I(className="bi bi-box-arrow-up-right")],
                    href="https://health-data-hub.shinyapps.io/dico-snds/",
                    target="_blank",
                ),
                dbc.NavLink(
                    ["Forum d'entraide ", html.I(className="bi bi-box-arrow-up-right")],
                    href="https://entraide.health-data-hub.fr/",
                    target="_blank",
                ),
                dbc.NavLink(
                    ["Répertoire public des projets ", html.I(className="bi bi-box-arrow-up-right")],
                    href="https://www.health-data-hub.fr/projets/",
                    target="_blank",
                ),
                html.Hr(),
                dcc.Markdown(
                    mds.MDS["WIP_banner.md"],
                    style={
                        "display": "inline-block",
                        "font-size": "80%",
                        "verticalAlign": "bottom",
                    },
                ),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    width={"size": 2, "order": 1},
    className="h-100",
    style={
        "overflow-y": "scroll",
        "position": "fixed",
        "backgroundColor": "#F7F4F9",
        "height": "100vh",
    },
)

page_intro: dbc.Col = dbc.Col(
    [
        html.Br(),
        cmp.updated_when_badge,
        dbc.Row(html.H1("Écosystème SNDS\t"), align="center"),
        dbc.Card(
            dcc.Markdown(mds.MDS["intro.md"]),
            body=True,
            color="#FFFFFF00",
            outline=True,
            style={"border": "0px", "backgroundColor": "#FFFFFF00"},
        ),
        dbc.Card(
            dbc.Row(
                [
                    dbc.Col(cmp.card_nbpubli),
                    dbc.Col(cmp.card_nbauth),
                    dbc.Col(cmp.card_nb_inst),
                    dbc.Col(cmp.card_nbcountries),
                    dbc.Col(cmp.card_meancit),
                ],
            ),
            body=True,
            color="light",
            outline=True,
            style={"backgroundColor": "#FFFFFF00"},
        ),
        html.A(id="query"),
        dbc.Card(
            [
                dbc.CardHeader(html.H5("Le Format des Requêtes")),
                dbc.CardBody(
                    dbc.Accordion(
                        [
                            dbc.AccordionItem(
                                dcc.Markdown(
                                    mds.MDS["PubMed_QUERY.md"],
                                    style={"font-family": "'Montserrat', sans-serif"},
                                    id="pubmed-query",
                                ),
                                title="PubMed",
                            ),
                            dbc.AccordionItem(
                                dcc.Markdown(
                                    mds.MDS["LiSSa_QUERY.md"],
                                    style={"font-family": "'Montserrat', sans-serif"},
                                    id="lissa-query",
                                ),
                                title="LiSSa",
                            ),
                            dbc.AccordionItem(
                                dcc.Markdown(
                                    mds.MDS["HAL_QUERY.md"],
                                    style={"font-family": "'Montserrat', sans-serif"},
                                    id="lissa-query",
                                ),
                                title="HAL",
                            ),
                        ],
                        flush=True,
                        start_collapsed=True,
                    ),
                ),
            ],
            color="light",
            outline=True,
        ),
        html.Br(),
        html.A(id="tutorial"),
        dbc.Card(
            dcc.Markdown(mds.MDS["user_guide.md"]),
            body=True,
            color="#FFFFFF00",
            outline=True,
            style={"border": "0px", "backgroundColor": "#FFFFFF00"},
        ),
        html.Br(),
        html.Br(),
        cmp.end_page_accordion,
    ],
)

app.layout = dbc.Container(
    [
        dbc.Row(
            [
                app_index,
                dcc.Location(id="url", refresh=False),
                dbc.Col(
                    id="page-content",
                    width={"size": 10, "order": 2, "offset": 2},
                    style={
                        "background-image": f"url('data:image/png;base64,{cst.png_files['Origami_1.png']}')",
                        "background-repeat": "no-repeat",
                        "background-position": "left top",
                        "background-size": "2000px 1000px",
                    },
                ),
            ],
        ),
    ],
    className="dbc",
    fluid=True,
)


@app.callback(
    Output("page-content", "children"),
    Input("url", "pathname"),
)
def display_page(pathname: str) -> dbc.Col:
    """callback displays correct page

    Args:
        pathname (str): the path

    Returns:
        dbc.Col: the dbc.Col object for the page
    """
    if pathname == "/publications":
        return pp.page_publications_layout
    elif pathname == "/auteurs":
        return ap.page_authors_layout
    elif pathname == "/structures":
        return ip.page_institutions_layout
    elif pathname == "/":
        return page_intro
    else:
        return page_intro


@app.callback(
    Output("landing", "children"),
    Output("navbar-publications", "children"),
    Output("navbar-authors", "children"),
    Output("navbar-structures", "children"),
    Input("url", "pathname"),
)
def navbar_landing(pathname: str):
    """callback displays correct page

    Args:
        pathname (str): the path

    Returns:
        dbc.Col: the dbc.Col object for the page
    """
    if pathname == "/publications":
        return [
            navlink_closed_intro,
            navlink_publications,
            navlink_closed_authors,
            navlink_closed_structures,
        ]
    elif pathname == "/auteurs":
        return [
            navlink_closed_intro,
            navlink_closed_publications,
            navlink_authors,
            navlink_closed_structures,
        ]
    elif pathname == "/structures":
        return [
            navlink_closed_intro,
            navlink_closed_publications,
            navlink_closed_authors,
            navlink_structures,
        ]
    elif pathname == "/":
        return [
            navlink_landing,
            navlink_closed_publications,
            navlink_closed_authors,
            navlink_closed_structures,
        ]
    else:
        return [
            navlink_landing,
        ]


@app.callback(
    Output("download-Publications-json", "data"),
    Input("menu-download-Publications-json", "n_clicks"),
    prevent_initial_call=True,
)
def download_publications_json(_):
    """dowloads publications.json

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.publications.to_json, "publications.json", indent=2)


@app.callback(
    Output("download-Publications-csv", "data"),
    Input("menu-download-Publications-csv", "n_clicks"),
    prevent_initial_call=True,
)
def download_publications_csv(_):
    """dowloads publications.csv

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.publications.to_csv, "publications.csv", sep=";")


@app.callback(
    Output("download-Publications-xlsx", "data"),
    Input("menu-download-Publications-xlsx", "n_clicks"),
    prevent_initial_call=True,
)
def download_publications_xlsx(_):
    """dowloads publications.xlsx

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.publications.to_excel, "publications.xlsx", index=False)


@app.callback(
    Output("download-Authors-json", "data"),
    Input("menu-download-Authors-json", "n_clicks"),
    prevent_initial_call=True,
)
def download_authors_json(_):
    """dowloads authors.json

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.authors.to_json, "authors.json", indent=2)


@app.callback(
    Output("download-Authors-csv", "data"),
    Input("menu-download-Authors-csv", "n_clicks"),
    prevent_initial_call=True,
)
def download_authors_csv(_):
    """dowloads authors.csv

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.authors.to_csv, "authors.csv", sep=";")


@app.callback(
    Output("download-Authors-xlsx", "data"),
    Input("menu-download-Authors-xlsx", "n_clicks"),
    prevent_initial_call=True,
)
def download_authors_xlsx(_):
    """dowloads authors.xlsx

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.authors.to_excel, "authors.xlsx", index=False)


@app.callback(
    Output("download-Structures-json", "data"),
    Input("menu-download-Structures-json", "n_clicks"),
    prevent_initial_call=True,
)
def download_structures_json(_):
    """dowloads ror_affiliations.json

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.ror_affiliations.to_json, "structures.json", indent=2)


@app.callback(
    Output("download-Structures-csv", "data"),
    Input("menu-download-Structures-csv", "n_clicks"),
    prevent_initial_call=True,
)
def download_structures_csv(_):
    """dowloads ror_affiliations.csv

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.ror_affiliations.to_csv, "structures.csv", sep=";")


@app.callback(
    Output("download-Structures-xlsx", "data"),
    Input("menu-download-Structures-xlsx", "n_clicks"),
    prevent_initial_call=True,
)
def download_structures_xlsx(_):
    """dowloads ror_affiliations.xlsx

    Args:
        indices (list[int]): indicates the order in which the original rows appear after being filtered and sorted

    Returns:
        dict of data frame content : dict of data frame content
    """
    return dcc.send_data_frame(cst.ror_affiliations.to_excel, "structures.xlsx", index=False)


if __name__ == "__main__":
    app.run_server(debug=False, host="0.0.0.0")
