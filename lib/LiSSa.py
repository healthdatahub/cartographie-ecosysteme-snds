from pathlib import Path
from typing import Any, Optional, Union

import pandas as pd


def reformat_authors_lissa(element: str) -> list[dict[str, Any]]:
    """reformats author column in LiSSa db

    Args:
        element (str): a row from column author

    Returns:
        list[dict[str,Any]]: a list of dicts
    """
    new = []
    for author in element:
        list_author = author.split(" ")
        temp = {}
        for index, item in enumerate(list_author):
            if index == 0:
                temp["LastName"] = item
            elif index == 1:
                temp["Initials"] = item[0].upper()
            elif index == 2:
                temp["Initials"] += item[0].upper()
        new.append(temp)
    return new


def reformat_keywords_lissa(element: str) -> Optional[list[dict[str, str]]]:
    """reformats author column in LiSSa db

    Args:
        element (str): a row from column author

    Returns:
        Optional[list[dict[str, str]]]: a list of dicts or None
    """
    new = []
    if isinstance(element, str):
        for keyword in element.split(";"):
            new.append({"@MajorTopicYN": "N", "#text": keyword})
        return new
    return None


def get_missing_articles() -> Union[pd.DataFrame, pd.Series]:
    """gets missing articles of LiSSa query in overall DB

    Returns:
        Union[pd.DataFrame, pd.Series]: the DataFrame only with missing articles of LiSSa query in overall DB
    """
    all_publi = pd.read_json(r"data/PubMed/pubmed_trimmed.json", orient="index")
    lissa_publi = pd.read_csv(r"data/LiSSa/LiSSa_2022-8-5.csv")
    lissa_dois = set(lissa_publi.DOI.dropna())
    mlist = list(lissa_dois - set(all_publi.index.dropna()))
    missing_lissa_publi = lissa_publi.loc[lissa_publi.DOI.isin(mlist)]
    return missing_lissa_publi


def format_lissa_db(missing_lissa_publi: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """formats LiSSa DB so its concated with full_results.json

    Args:
        missing_lissa_publi (Union[pd.DataFrame, pd.Series]): the DataFrame only with missing articles of LiSSa query in overall DB

    Returns:
        Union[pd.DataFrame, pd.Series]: the PubMed + LiSSa DataFrame
    """
    all_res = pd.read_json(r"data/PubMed/pubmed_trimmed.json", orient="index")
    missing_lissa_publi.Auteurs = missing_lissa_publi.Auteurs.str.replace(",", "", regex=False).str.replace(".", "", regex=False).str.split(";")
    missing_lissa_publi = missing_lissa_publi.drop(
        [
            "Volume",
            "Numéro",
            "Pages",
            "Éditeur",
            "ISSN",
        ],
        axis=1,
    )
    missing_lissa_publi = missing_lissa_publi.rename(
        columns={
            "Titre": "ArticleTitle",
            "Auteurs": "AuthorList",
            "Année": "PubYear",
            "Journal": "Journal-Title",
            "DOI": "doi",
            "Mots-clés": "KeywordList",
            "Résumé": "Abstract",
        }
    )  # type: ignore
    missing_lissa_publi["PubDate"] = missing_lissa_publi["PubYear"].apply(str)
    missing_lissa_publi["SortPubDate"] = missing_lissa_publi["PubDate"]
    missing_lissa_publi["PmcRefCount"] = None
    missing_lissa_publi["MeshHeadingList"] = None
    missing_lissa_publi["articleDB"] = "LiSSa"
    missing_lissa_publi["VernacularTitle"] = None
    missing_lissa_publi["Journal-ISOAbbreviation"] = None
    missing_lissa_publi["PMID"] = None
    missing_lissa_publi["Language"] = "fre"
    missing_lissa_publi["ArticleType"] = "Article dans une revue"
    missing_lissa_publi.AuthorList = missing_lissa_publi.AuthorList.apply(reformat_authors_lissa)
    missing_lissa_publi.KeywordList = missing_lissa_publi.KeywordList.apply(reformat_keywords_lissa)
    addlissa = missing_lissa_publi.set_index("doi")
    new_full_res = pd.concat([all_res, addlissa])
    new_full_res = new_full_res[~new_full_res.index.duplicated(keep="first")]
    new_full_res.reset_index()
    new_full_res.PMID = new_full_res["PMID"].astype(str)
    new_full_res.PMID = new_full_res["PMID"].apply(lambda x: None if x == "None" else x)
    return new_full_res


def main():
    """main func"""
    print("adding LiSSa")
    missing_lissa_publi = get_missing_articles()
    path = Path("data/resources/")
    path.mkdir(parents=True, exist_ok=True)
    all_db = format_lissa_db(missing_lissa_publi)
    all_db.to_json(path / "all_DB.json", indent=2, orient="index")
