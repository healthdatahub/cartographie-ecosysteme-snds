import datetime
import glob
import json
import re
import xml.etree.ElementTree as ET
from pathlib import Path
from typing import Any, Optional, OrderedDict

import requests
import xmltodict
from dateutil import parser
from natsort import natsorted
from tqdm import tqdm

import lib.pubquery

QUERY = lib.pubquery.QUERY
QUERY_BIS = lib.pubquery.QUERY_BIS
URL = "https://eutils.ncbi.nlm.nih.gov"
TOOL = "cartographie-ecosysteme-snds"
EMAIL = "opensource@health-data-hub.fr"
PATH = Path("data/PubMed/")
EUTILITY = "entrez/eutils/esearch.fcgi"


def chunkify(tobchunked: list[Any], num_items: int) -> list[list[Any]]:
    """transforms list into list of chunks

    Args:
        tobchunked (list[Any]): list to be chunked
        num_items (int): number of items in chunks wanted

    Returns:
        list[list[Any]]: chunked list
    """
    return [tobchunked[i : i + num_items] for i in range(0, len(tobchunked), num_items)]


def get_request(url: str, eutility: str, parameters: dict[str, Any]) -> requests.Response:
    """generic get request

    Args:
        url (str): base url
        eutility (str): name of extended url
        parameters (dict[str, Any]): parameters

    Returns:
        requests.Response: the get request response
    """
    return requests.get(url=f"{url}/{eutility}", params=parameters)


def post_request(url: str, eutility: str, parameters: dict[str, Any]) -> requests.Response:
    """generic get request

    Args:
        url (str): base url
        eutility (str): name of extended url
        parameters (dict[str, Any]): parameters

    Returns:
        requests.Response: the post request response
    """
    try:
        response = requests.post(url=f"{url}/{eutility}", params=parameters)
        response.raise_for_status()
    except requests.exceptions.HTTPError as err:
        return err
    return response


def do_xmltodict(file_: str) -> OrderedDict[str, Any]:
    """does xmltodict

    Args:
        file_ (str): filepath

    Returns:
        OrderedDict[str, Any]: xmltodict
    """
    tree = ET.parse(file_)
    root = tree.getroot()
    xml_str = ET.tostring(root, encoding="unicode")
    return xmltodict.parse(xml_str)


def get_pmid(article: dict[str, Any]) -> Optional[str]:
    """gets PMID object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or PMID
    """
    try:
        res = article["MedlineCitation"]["PMID"]["#text"]
    except KeyError:
        res = None
    return res


def get_journal_title(article: dict[str, Any]) -> Optional[str]:
    """gets journal title object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or _replace_
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["Journal"]["Title"]
    except KeyError:
        res = None
    return res


def get_journal_isoabbreviation(article: dict[str, Any]) -> Optional[str]:
    """gets journal_isoabbreviation object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or journal_isoabbreviation
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["Journal"]["ISOAbbreviation"]
    except KeyError:
        res = None
    return res


def get_articletitle(article: dict[str, Any]) -> Optional[str]:
    """gets articletitle object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or articletitle
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["ArticleTitle"]
    except KeyError:
        res = None
    return res


def get_vernaculartitle(article: dict[str, Any]) -> Optional[str]:
    """gets vernaculartitle object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or vernaculartitle
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["VernacularTitle"]
    except KeyError:
        res = None
    return res


def get_abstract(article: dict[str, Any]) -> Optional[dict[str, Any]]:
    """gets abstract object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[dict[str, Any]]: None or abstract
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["Abstract"]["AbstractText"]
    except KeyError:
        res = None
    return res


def get_authorlist(article: dict[str, Any]) -> Optional[dict[str, Any]]:
    """gets authorlist object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[dict[str, Any]]: None or authorlist
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["AuthorList"]["Author"]
    except KeyError:
        res = None
    return res


def get_language(article: dict[str, Any]) -> Optional[str]:
    """gets language object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or language
    """
    short = article["MedlineCitation"]["Article"]
    try:
        res = short["Language"]
    except KeyError:
        res = None
    return res


def get_meshheadinglist(article: dict[str, Any]) -> Optional[dict[str, Any]]:
    """gets meshheadinglist object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[dict[str, Any]]: None or meshheadinglist
    """
    try:
        res = article["MedlineCitation"]["MeshHeadingList"]["MeshHeading"]
    except KeyError:
        res = None
    return res


def get_keywordlist(article: dict[str, Any]) -> Optional[dict[str, Any]]:
    """gets keywordlist object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[dict[str, Any]]: None or keywordlist
    """
    try:
        res = article["MedlineCitation"]["KeywordList"]["Keyword"]
    except KeyError:
        res = None
    return res


def get_pmcrefcount(article: dict[str, Any]) -> Optional[int]:
    """gets pmcrefcount object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[int]: None or pmcrefcount
    """
    try:
        res = int(article["DocumentSummary"]["PmcRefCount"])
    except KeyError:
        res = None
    return res


def get_pubdate(article: dict[str, Any]) -> Optional[str]:
    """gets pubdate object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or pubdate
    """
    try:
        res = article["DocumentSummary"]["PubDate"]
    except KeyError:
        res = None
    return res


def get_sortpubdate(article: dict[str, Any]) -> Optional[str]:
    """gets sortpubdate object from result dict

    Args:
        article (dict[str, Any]): article dict in res result

    Returns:
        Optional[str]: None or sortpubdate
    """
    try:
        res = article["DocumentSummary"]["SortPubDate"]
    except KeyError:
        res = None
    return res


def reformat_affiliations(author) -> Optional[list[str]]:
    """_summary_"""
    try:
        if isinstance(author["AffiliationInfo"], dict):
            affiliations = list(author["AffiliationInfo"].values())
        elif isinstance(author["AffiliationInfo"], list):
            affiliations = []
            for aff in author["AffiliationInfo"]:
                affiliations.append(aff["Affiliation"])
        else:
            return None
        return affiliations
    except KeyError:
        return None


class PubmedQuery:
    """Class contains methods to query PubMed for new SNDS Articles"""

    def __init__(self, path: Path, query: str, base_url: str, tool: str, email: str) -> None:
        """contructor initializes key variables

        Args:
            path (Path): the path to the local repo
            query (str): the QUERY used to get SNDS Articles
            base_url (str): PubMed's base URL
            tool (str): name of the tool to comply with PubMed's API guidelines
            email (str): email of main user to comply with PubMed's API guidelines
        """
        self.path = path
        date = datetime.datetime.today()
        self.date = f"{date.day}-{date.month}-{date.year}"
        self.base_url = base_url
        self.tool = tool
        self.email = email
        self.query = query
        self.pmids: list[str] = []
        self.articles: dict[str, Any] = {}
        self.set_parameters()

    def set_parameters(self) -> None:
        """resets dict variables for second batches of querying"""
        self.parameters: dict[str, Any] = {
            "tool": self.tool,
            "email": self.email,
            "db": "pubmed",
        }

    def query_pubmed_get_ids(self) -> None:
        """gets all the PubMed SNDS Article ids

        Returns:
            requests.models.Response: the response to the query
        """
        self.parameters["term"] = self.query
        self.parameters["retmax"] = 50000
        self.parameters["format"] = "json"
        eutility = "entrez/eutils/esearch.fcgi"
        response = get_request(self.base_url, eutility, self.parameters)
        self.pmids = response.json()["esearchresult"]["idlist"]
        print(f"found {len(self.pmids)} articles in PubMed")

    def get_articles(self) -> None:
        """gets articles data from pubmed"""
        self.set_parameters()
        self.parameters["version"] = 2.0
        self.parameters["retmode"] = "xml"
        Path(self.path / "efetches").mkdir(parents=True, exist_ok=True)
        Path(self.path / "esummaries").mkdir(parents=True, exist_ok=True)
        array_request_pubmed_ids = chunkify(self.pmids, 200)
        request_pubmed_ids = [",".join(chunk) for chunk in array_request_pubmed_ids]
        request_pubmed_ids = list(filter(None, request_pubmed_ids))
        for index, chunk in tqdm(
            enumerate(request_pubmed_ids),
            desc="efetches & esummaries",
            total=len(request_pubmed_ids),
        ):
            self.parameters["id"] = chunk
            response = post_request(self.base_url, "entrez/eutils/efetch.fcgi?", self.parameters)
            tree = ET.ElementTree(ET.fromstring(response.text))
            ET.indent(tree, space="\t", level=0)
            tree.write(Path(self.path / "efetches" / f"efetch{index}.xml"), encoding="utf-8")
            response = post_request(self.base_url, "entrez/eutils/esummary.fcgi?", self.parameters)
            tree = ET.ElementTree(ET.fromstring(response.text))
            ET.indent(tree, space="\t", level=0)
            tree.write(Path(self.path / "esummaries" / f"esummary{index}.xml"), encoding="utf-8")

    def construct_pubmed_database(self) -> None:
        """converts xmls to one json file"""
        esummaries = natsorted(glob.glob(f"{self.path}/esummaries/*.xml"))
        for index, file_ in enumerate(
            tqdm(
                natsorted(glob.glob(f"{self.path}/efetches/*.xml")),
                desc="Translating to Json",
                total=len(esummaries),
            )
        ):
            efetch = do_xmltodict(file_)
            esummary = do_xmltodict(esummaries[index])
            for mark, value in enumerate(efetch["PubmedArticleSet"]["PubmedArticle"]):
                try:
                    ELocationID = value["MedlineCitation"]["Article"]["ELocationID"]
                    key = "@EIdType"
                except KeyError:
                    ELocationID = value["PubmedData"]["ArticleIdList"]["ArticleId"]
                    key = "@IdType"
                doi = None
                if isinstance(ELocationID, dict):
                    doi = ELocationID["#text"]
                else:
                    for EIdType in ELocationID:
                        if EIdType[key] == "doi":
                            doi = EIdType["#text"]
                            break
                if doi:
                    value["DocumentSummary"] = esummary["eSummaryResult"]["DocumentSummarySet"]["DocumentSummary"][mark]
                    self.articles[doi] = value
        with open(Path(self.path / "pubmed_raw.json"), "w", encoding="utf-8") as file_:
            json.dump(self.articles, file_, indent=2)

    def parser(self) -> None:
        """parses the raw query results to get the good info"""
        with open(Path(self.path / "pubmed_raw.json"), "r", encoding="utf-8") as file_:
            articles_raw = json.load(file_)
        trimmed_article: dict[str, dict[str, Any]] = {}
        for doi in tqdm(
            articles_raw,
            desc="Parsing",
            total=len(articles_raw),
        ):
            trimmed_article[doi] = {}
            article = articles_raw[doi]
            trimmed_article[doi]["PMID"] = get_pmid(article)
            trimmed_article[doi]["Journal-Title"] = get_journal_title(article)
            trimmed_article[doi]["Journal-ISOAbbreviation"] = get_journal_isoabbreviation(article)
            trimmed_article[doi]["ArticleTitle"] = get_articletitle(article)
            try:
                trimmed_article[doi]["ArticleTitle"] = trimmed_article[doi]["ArticleTitle"]["#text"]
            except TypeError:
                pass
            trimmed_article[doi]["VernacularTitle"] = get_vernaculartitle(article)
            trimmed_article[doi]["Abstract"] = get_abstract(article)
            trimmed_article[doi]["AuthorList"] = get_authorlist(article)
            try:
                for author in trimmed_article[doi]["AuthorList"]:
                    author["AffiliationInfo"] = reformat_affiliations(author)
            except TypeError:
                pass
            trimmed_article[doi]["Language"] = get_language(article)
            trimmed_article[doi]["MeshHeadingList"] = get_meshheadinglist(article)
            trimmed_article[doi]["KeywordList"] = get_keywordlist(article)
            trimmed_article[doi]["PmcRefCount"] = get_pmcrefcount(article)
            trimmed_article[doi]["PubDate"] = get_pubdate(article)
            try:
                trimmed_article[doi]["PubYear"] = str(parser.parse(trimmed_article[doi]["PubDate"], fuzzy=True).year)
            except parser.ParserError:
                trimmed_article[doi]["PubYear"] = re.search(r"\d{4}", trimmed_article[doi]["PubDate"]).group(0)
            trimmed_article[doi]["SortPubDate"] = get_sortpubdate(article)
            trimmed_article[doi]["articleDB"] = "PubMed"
            trimmed_article[doi]["ArticleType"] = "Article dans une revue"
        with open(Path(self.path / "pubmed_trimmed.json"), "w", encoding="utf-8") as file_:
            json.dump(trimmed_article, file_, indent=2)


def main() -> None:
    """main func"""
    query = PubmedQuery(PATH, QUERY, URL, TOOL, EMAIL)
    query_bis = PubmedQuery(PATH, QUERY_BIS, URL, TOOL, EMAIL)
    query.query_pubmed_get_ids()
    query_bis.query_pubmed_get_ids()
    query.pmids.extend(query_bis.pmids)
    query.get_articles()
    query.construct_pubmed_database()
    query.parser()
