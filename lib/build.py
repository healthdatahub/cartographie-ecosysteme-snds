"""builds data bases"""

import json
from pathlib import Path
from typing import Any, Optional, Union

import networkx as nx
import pandas as pd
import unidecode

if __name__ == "__main__":
    import keywords
    import mesh
    import network
    import plots
    import ror
else:
    from lib import keywords, mesh, network, plots, ror


DEFAULT_AUTHOR = "Plancke L"
DEFAULT_STRUCTURE = "ADERA"


def get_key(element: Optional[dict], key: str) -> Optional[Any]:
    """gets key from a dict if element a dict

    Args:
        element (Optional[dict]): element from pandas
        key (str): key in dict

    Returns:
        Optional[Any]: None or value of key
    """
    if isinstance(element, dict):
        try:
            return element[key]
        except KeyError:
            return None
    return None


def build_publications(all_db: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """builds publications

    Args:
        all_db (pd.DataFrame): the results of LiSSa + PubMed DBs

    Returns:
        Union[pd.DataFrame, pd.Series]: _description_
    """
    all_db.index.names = ["doi"]
    all_db = all_db.reset_index()
    return all_db


def explode_authors(publications: Union[pd.DataFrame, pd.Series]) -> pd.DataFrame:
    """does the main exploded authors db

    Args:
        publications(Union[pd.DataFrame, pd.Series]): the output from build_publications(*args)

    Returns:
        pd.DataFrame: exploded authors DataFrame
    """
    exp_autho = publications.explode(column="AuthorList")  # type: ignore
    for key in ["LastName", "Initials", "ForeName", "Identifier", "AffiliationInfo"]:
        exp_autho[key] = exp_autho.AuthorList.apply(lambda x: get_key(x, key))
    exp_autho = exp_autho.drop(columns=["AuthorList"])
    exp_autho["FullName"] = exp_autho["LastName"] + " " + exp_autho["Initials"]
    exp_autho["NormalizedName"] = exp_autho["LastName"] + "." + exp_autho["Initials"]
    exp_autho["NormalizedName"] = exp_autho["NormalizedName"].apply(
        lambda x: unidecode.unidecode(x.replace("-", ".").replace(" ", ".").lower()) if isinstance(x, str) else None
    )
    return exp_autho.reset_index()


def build_authors(authors: pd.DataFrame) -> Union[pd.DataFrame, pd.Series]:
    """function takes output from do_exploded_authors(*args) returns a grouped by author Dataframe

    Args:
        authors (pd.DataFrame): the exploded authors DataFrame

    Returns:
        pd.DataFrame: a new DataFrame grouped by "Normalized Name"
    """
    data = (
        authors.groupby(["NormalizedName"])
        .agg(
            {
                "FullName": lambda x: "; ".join(list(dict.fromkeys(x))),
                "PMID": list,
                "doi": list,
                "LastName": set,
                "Initials": set,
                "ForeName": set,
                "Identifier": lambda x: [item for item in x if item],
                "AffiliationInfo": list,
            }
        )
        .reset_index()
    )
    for col in data.columns:
        data[col] = data[col].apply(lambda x: None if all(item is None for item in x) else x)
    return data


def build_graphs(
    dataframe: Union[pd.DataFrame, pd.Series],
    path: Path,
    info: dict[str, str],
    default_node: str,
    depth_limit: int = 1,
) -> None:
    """Builds the network graph of {category} DataFrame with a {node} column, {edge} column

    Args:
        dataframe (Union[pd.DataFrame, pd.Series]): a pandas dataframe
        path(str)
        info(dict[str, str]):
            node (str): name of node column
            edge (str): name of edge column
            category (str): name of the DataFrame
        default_node (str): default node chosen
        depth_limit (int): default depth limit chosen. Default value 2
    """
    node = info["node"]
    edge = info["edge"]
    category = info["category"]
    dataframe = dataframe.explode(column=edge)  # type: ignore
    dataframe = dataframe[dataframe[node].notnull()]
    dataframe = dataframe[dataframe[node] != ""]
    full_graph = network.create_full_graph(dataframe, node, edge)
    Path(path / f"graphs/{category}/").mkdir(parents=True, exist_ok=True)
    nx.write_gml(full_graph, path / f"graphs/{category}/{node}_{edge}_graph.gml")
    assembled_aff_graph, _, _ = network.sub_graph(
        full_graph,
        default_node,
        depth_limit=depth_limit,
    )
    with open(Path(f"data/resources/graphs/{category}/{default_node}_{depth_limit}_{node}_{edge}_subgraph.json"), "w", encoding="utf-8") as file_:
        json.dump(assembled_aff_graph, file_, indent=2)


class Builder:
    """builds data"""

    def __init__(self) -> None:
        """constructor calls all subsequent methods"""
        self.path = Path("data/resources/")
        self.path.mkdir(parents=True, exist_ok=True)
        self.all_db = pd.read_json(self.path / "all_DB.json").T
        self.publications = build_publications(self.all_db)

        print("Building exploded_authors.json")
        self.exploded_authors = explode_authors(self.publications)
        self.exploded_authors.to_json(self.path / "exploded_authors.json", indent=2)

        print("Building authors.json")
        self.authors = build_authors(self.exploded_authors)
        self.authors.to_json(self.path / "authors.json", indent=2)

        print("Building ror_affiliations.json")
        self.rorsearch = ror.ROR(exploded_authors=self.exploded_authors, path=self.path)
        self.rorsearch.convert_previous_results()
        self.rorsearch.get_previous_ror_results()
        self.rorsearch.do_full_ror_search()
        self.rorsearch.ror_to_df()
        self.rorsearch.add_article_info()
        self.rorsearch.ror_df.to_json(self.path / "ror_affiliations.json", indent=2)
        self.rorsearch.affs.to_json(self.rorsearch.prev_file, indent=2)

        print("Building publications.json")
        self.publications = self.publications.set_index("doi")
        self.publications["FullName"] = pd.merge(self.publications, self.exploded_authors, on="doi").groupby("doi").agg({"FullName": list}).FullName
        self.publications["AffiliationInfo"] = (
            pd.merge(self.publications, self.rorsearch.ror_df.explode("doi"), on="doi").groupby("doi").agg({"ror": list}).ror
        )
        self.publications = self.publications.reset_index()
        self.publications.to_json(self.path / "publications.json", indent=2)

        self.keywords_and_meshs()
        self.graphs_caller()
        self.plots_caller()

    def keywords_and_meshs(self) -> None:
        """calls all functions to build keywords & mesh DataFrames"""
        print("Building raw_keywords.json")
        keyword_stats = keywords.keyword_stats(self.all_db)  # type: ignore
        keyword_stats.to_json(self.path / "raw_keywords.json", indent=2)

        print("Building keyword_freqs.json")
        key_freqs = keywords.keyword_freqs(keyword_stats)
        key_freqs.to_json(self.path / "keyword_freqs.json", indent=2)

        print("Building raw_mesh.json")
        mesh_stats = mesh.mesh_stats(self.all_db)  # type: ignore
        mesh_stats.to_json(self.path / "raw_mesh.json", indent=2)

        print("Building uniform_mesh_freqs.json")
        uniform_mesh_freqs = mesh.mesh_full_stats(mesh_stats)  # type: ignore
        uniform_mesh_freqs.to_json(self.path / "uniform_mesh_freqs.json", indent=2)

        print("Building main_mesh_freqs.json")
        main_mesh_freqs = mesh.main_mesh_stats(mesh_stats)
        main_mesh_freqs.to_json(self.path / "main_mesh_freqs.json", indent=2)

        plots.build_wordcoulds(uniform_mesh_freqs, main_mesh_freqs, key_freqs)

    def graphs_caller(self) -> None:
        """calls all functions to build nteworkx graphs"""
        print("Building authors graphs")
        info = {"node": "FullName", "edge": "doi", "category": "authors"}
        build_graphs(self.exploded_authors, self.path, info, DEFAULT_AUTHOR, depth_limit=2)

        print("Building structures graphs")
        info = {"node": "ror", "edge": "doi", "category": "structures"}
        build_graphs(self.rorsearch.ror_df, self.path, info, DEFAULT_STRUCTURE)

    def plots_caller(self) -> None:
        """calls all functions to make plot DataFrames"""
        Path(self.path / "plots").mkdir(parents=True, exist_ok=True)
        print("Building countries_of_structures_per_article.json")
        countryspa = plots.build_countries_of_structures_per_article(self.rorsearch.ror_df.explode("PMID"))
        countryspa.to_json(self.path / "plots/countries_of_structures_per_article.json")

        print("Building cities_of_structures_per_article.json")
        cityspa = plots.build_cities_of_structures_per_article(self.rorsearch.ror_df.explode("PMID"))
        cityspa.to_json(self.path / "plots/cities_of_structures_per_article.json")

        print("Building map_world_structure_publications.json")
        wsp = plots.build_map_world_structure_publications(self.rorsearch.ror_df.explode("PMID"))
        wsp.to_json(self.path / "plots/map_world_structure_publications.json")

        print("Building publications_per_institution.json")
        pps = plots.build_publications_per_institution(self.rorsearch.ror_df.to_dict("records"))
        with open(self.path / "plots/publications_per_institution.json", "w", encoding="utf-8") as file_:
            json.dump(pps, file_, indent=2)

        print("Building publications_per_journal.json")
        ppj = plots.build_publications_per_journal(self.all_db.to_dict("records"))
        with open(self.path / "plots/publications_per_journal.json", "w", encoding="utf-8") as file_:
            json.dump(ppj, file_, indent=2)

        print("Building publications_per_year.json")
        ppy = plots.build_publications_per_year(self.all_db.to_dict("records"))
        with open(self.path / "plots/publications_per_year.json", "w", encoding="utf-8") as file_:
            json.dump(ppy, file_, indent=2)

        print("Building publications_per_year_per_journal.json")
        ppypj = plots.build_publications_per_year_per_journal(self.publications)
        ppypj.to_json(self.path / "plots/publications_per_year_per_journal.json", orient="records")

        print("Building lang_pie.json")
        lang = plots.build_lang_pie(self.all_db)
        lang.to_json(self.path / "plots/lang_pie.json")

        print("Building publications_per_lang_per_year.json")
        lang = plots.build_publications_per_lang_per_year(self.publications)
        lang.to_json(self.path / "plots/publications_per_lang_per_year.json")


if __name__ == "__main__":
    Builder()
