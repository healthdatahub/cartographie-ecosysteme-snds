import json
import random
from collections import Counter
from typing import Any, Optional, Union

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import pycountry
from wordcloud import WordCloud

BG_COLOR = "rgba(242, 247, 249, 1)"


def wordcloud_color_func(word, font_size, position, orientation, random_state=None, **kwargs) -> str:
    """world cloud color function

    Args:
        word (_type_): unusued
        font_size (_type_): unusued
        position (_type_): unusued
        orientation (_type_): unusued
        random_state (_type_, optional): unusued. Defaults to None.

    Returns:
        str: a random color from a list of colors
    """
    return random.choice(["#ffd60c", "#036287", "#abadff", "#5deee3", "#000091", "#a272ff", "#65c5ae", "#06303a", "#2570f1", "#612491"])


def create_dict(element: pd.Series) -> dict[str, int]:
    """creates a sorted dict from 2 columns

    Args:
        element (pd.Series): a row

    Returns:
        dict[str, int]: a dict
    """
    count = Counter(dict(zip(element.PMID, element.PubYear)).values())
    return dict(sorted(count.items()))


def build_publications_per_lang_per_year(
    publications: Union[pd.DataFrame, pd.Series],
) -> Union[pd.DataFrame, pd.Series]:
    """builds publications_per_lang_per_year

    Args:
        publications (Union[pd.DataFrame, pd.Series]): the publications.json

    Returns:
        Union[pd.DataFrame, pd.Series]: the langague counts per year per
    """
    lang = publications[["Language", "PubYear"]].explode("Language").groupby("Language").agg({"PubYear": list})
    lang.PubYear = lang.PubYear.apply(lambda x: dict(sorted(Counter(x).items())))
    lang["Year"] = lang.PubYear.apply(lambda x: list(x.keys()))
    lang["Count"] = lang.PubYear.apply(lambda x: list(x.values()))
    lang = lang.drop(columns="PubYear")
    lang = lang.explode(["Year", "Count"]).reset_index()
    lang = lang[~lang.Language.isin(["ger", "gre", "ita"])]
    return lang


def plot_publications_per_lang_per_year() -> px.line:
    """plots the publications_per_year_per_journal
    
    Returns:
        px.line: the publications_per_lang_per_year plot"""
    lang = pd.read_json(r"data/resources/plots/publications_per_lang_per_year.json")
    fig = px.line(
        lang,
        x="Year",
        y="Count",
        color="Language",
        color_discrete_sequence=[
            "#036287",
            "#612491",
        ],
    )
    newnames = {"eng": "Anglais", "fre": "Français"}
    fig.update_layout(
        plot_bgcolor=BG_COLOR,
        xaxis={"title": "Années"},
        yaxis={"title": "Compte"},
    )
    fig.for_each_trace(
        lambda t: t.update(
            name=newnames[t.name],
            legendgroup=newnames[t.name],
            hovertemplate=t.hovertemplate.replace(t.name, newnames[t.name]),
        )
    )
    return fig


def build_publications_per_year_per_journal(
    publications: Union[pd.DataFrame, pd.Series],
) -> Union[pd.DataFrame, pd.Series]:
    """builds the dataframe for the publications per year per journal

    Args:
        publications (Union[pd.DataFrame, pd.Series]): the publications.json

    Returns:
        dict: 4 keys ["Journal-Title", "PubYear", "Count", "CumSum"]
    """
    journals = (
        publications.groupby("Journal-Title")
        .agg(
            {
                "doi": list,
                "PMID": list,
                "Journal-ISOAbbreviation": "first",
                "PubDate": list,
                "PubYear": list,
                "SortPubDate": list,
            }
        )
        .reset_index()
    )
    journals["data"] = journals.apply(create_dict, axis=1)
    lines = pd.DataFrame()
    lines["Journal-Title"] = journals["Journal-Title"]
    lines["PubYear"] = journals.data.apply(lambda x: list(x.keys()))
    lines["Count"] = journals.data.apply(lambda x: list(x.values()))
    lines["CumSum"] = lines.Count.apply(np.cumsum)
    lines = lines.explode(["PubYear", "Count", "CumSum"])
    counts = publications["Journal-Title"].value_counts().to_dict()
    nlines = lines.loc[lines["Journal-Title"].isin(list(counts.keys()))]
    return nlines


def plot_publications_per_year_per_journal(data: Union[pd.DataFrame, pd.Series], cumsum: bool) -> px.line:
    """plots the publications_per_year_per_journal

    Args:
        data (Union[pd.DataFrame, pd.Series]): dataframe or subdataframe of Journal-Titles
        cumsum (bool): if True will use CumSum not Count

    Returns:
        px.line: the publications_per_year_per_journal plot
    """
    if cumsum:
        ytitle = "Compte Cumulé"
        ycol = "CumSum"
    else:
        ytitle = "Compte"
        ycol = "Count"

    fig = px.line(
        pd.DataFrame(data),
        x="PubYear",
        y=ycol,
        color="Journal-Title",
    )
    fig.update_layout(
        yaxis_title=ytitle,
        yaxis=dict(tickmode="array"),
        xaxis_title="Années",
        xaxis_range=[2007, 2022],
        plot_bgcolor=BG_COLOR,
    )
    return fig


def build_wordcoulds(
    uniform_mesh_freqs: Union[pd.DataFrame, pd.Series],
    main_mesh_freqs: Union[pd.DataFrame, pd.Series],
    keyword_freqs: Union[pd.DataFrame, pd.Series],
) -> None:
    """builds wordclouds of mesh & keywords

    Args:
        uniform_mesh_freqs (Union[pd.DataFrame, pd.Series])
        main_mesh_freqs (Union[pd.DataFrame, pd.Series])
        keyword_freqs (Union[pd.DataFrame, pd.Series])
    """
    paths = []
    wordcoud_data = {
        "FullMeSH": ["data/resources/allMeSH_wordcloud.png", uniform_mesh_freqs],
        "MainMeSH": ["data/resources/mainMeSH_wordcloud.png", main_mesh_freqs],
        "NormalizedKeywords": ["data/resources/keyword_wordcloud.png", keyword_freqs],
    }
    for key, value in wordcoud_data.items():
        data = value[1]
        column = key
        path_ = value[0]
        print(f"Building {column} Wordcloud")
        data = data.to_dict("list")
        plot_wordcloud(data, column, path_)
        paths.append(path_)


def plot_wordcloud(data: dict[str, Any], column: str, save_path: Optional[str] = None) -> None:
    """plots wordcould

    Args:
        data (dict[str, Any]): the dataframe containing wordcloud future data
        column (str): the column to get the word frequencies from
        save_path (Optional[str], optional): a path on which to save wordcloud. Defaults to None.
    """
    keys = data[column]
    values = data["Count"]
    words_frequencies = {key: values[index] for index, key in enumerate(keys)}
    wordcloud = WordCloud(
        width=800, height=800, background_color="white", min_font_size=10, color_func=wordcloud_color_func
    ).generate_from_frequencies(words_frequencies)
    # plot the Wordcloud image
    plt.figure(figsize=(8, 8), facecolor=None)
    plt.imshow(wordcloud, cmap="inferno")
    plt.axis("off")
    plt.tight_layout(pad=0)
    if save_path is not None:
        plt.savefig(save_path)
    else:
        plt.show()


def build_publications_per_year(data: Union[list[dict[str, Any]], dict[Any, Any]]) -> dict[str, Any]:
    """builds and writes to local file the data for plotting data_publications_per_year

    Args:
        data (Union[list[dict[str, Any]], dict[Any, Any]]): dict containing publication date key
    """
    dates = [item["PubYear"] for item in data]
    ycounts: Union[Counter, dict] = Counter(dates)
    ycounts = dict(sorted(ycounts.items()))
    fdata = {"Années": list(ycounts.keys()), "Nombre de publications SNDS": list(ycounts.values())}
    return fdata


def plot_publications_per_year() -> px.line:
    """plots publications per year from built local data

    Returns:
        px.line: plotly line figure publications per year
    """
    with open(r"data/resources/plots/publications_per_year.json", "r", encoding="utf-8") as file_:
        ycounts = json.load(file_)
    fig = px.line(ycounts, x="Années", y="Nombre de publications SNDS")
    fig.update_traces(line_color="#036287")
    fig.update_layout(plot_bgcolor="rgba(242, 247, 249, 1)")
    return fig


def build_publications_per_journal(data: Union[list[dict[str, Any]], dict[Any, Any]], cutoff: int = 25) -> dict[str, Any]:
    """builds and witres to local file the data for plotting data_publications_per_journal

    Args:
        data (Union[list[dict[str, Any]], dict[Any, Any]]): dict containing journal key
        cutoff (int, optional): a cutoff to show only the first X. Defaults to 25.
    """
    all_journals = [item["Journal-Title"] for item in data]
    jcounts: Union[Counter, dict] = Counter(all_journals)
    filtered = {k: v for k, v in jcounts.items()}
    jcounts = dict(sorted(filtered.items(), key=lambda kv: kv[1], reverse=True))
    jcounts = {"Journaux": list(jcounts.keys())[:cutoff], "Nombre de publications SNDS": list(jcounts.values())[:cutoff]}
    return jcounts


def plot_publications_per_journal(cutoff: int = 25) -> px.bar:
    """plots publications per journal from built local data

    Args:
        cutoff (int, optional): a cutoff to show only the first X. Defaults to 25.

    Returns:
        px.bar: plotly bar figure number of publications per journal
    """
    jcounts = pd.read_json(r"data/resources/plots/publications_per_journal.json")[:cutoff]
    fig = px.bar(jcounts, x="Journaux", y="Nombre de publications SNDS")
    fig.update_traces(marker_color="#036287")
    fig.update_layout(xaxis_tickangle=-60, height=900, plot_bgcolor=BG_COLOR)
    return fig


def build_countries_of_structures_per_article(data: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """builds and witres to local file the data for plotting countries_of_structures_per_article

    Args:
        data (Union[pd.DataFrame, pd.Series]): dict containing journal key
        cutoff (int, optional): a cutoff to show only the first X. Defaults to 25.
    """
    cocounts = Counter(data.country.apply(lambda x: x["country_name"]))
    names = list(cocounts.keys())
    names.append("Autre")
    names.append("Total")
    values = list(cocounts.values())
    other = 0
    for item in cocounts.items():
        if item[0] != "France":
            other += item[1]
    values.append(other)
    values.append(sum(cocounts.values()))
    parents = ["Total" if country in ["France", "Autre"] else "" if country in ["Total"] else "Autre" for country in names]
    fdata = {
        "names": names,
        "values": values,
        "parents": parents,
    }
    return pd.DataFrame(fdata)


def plot_countries_of_structures_per_article() -> px.sunburst:
    """plots sunburst author per country

    Returns:
        px.sunburst: plotly sunburst plot of country per author
    """
    fdata = pd.read_json(r"data/resources/plots/countries_of_structures_per_article.json")
    fig = px.sunburst(
        fdata,
        names="names",
        parents="parents",
        values="values",
        branchvalues="total",
        color_discrete_sequence=["#036287", "#612491"],
    )
    fig.update_layout(margin=dict(t=0, l=0, r=0, b=0))
    fig.update_traces(textinfo="label+percent entry")
    return fig


def build_cities_of_structures_per_article(data: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """builds and witres to local file the data for plotting cities_of_structures_per_article

    Args:
        data (Union[pd.DataFrame, pd.Series]): dict containing journal key
        cutoff (int, optional): a cutoff to show only the first X. Defaults to 25.
    """
    cicounts = Counter(data.addresses.apply(lambda x: x[0]["city"]))
    names = list(cicounts.keys())
    names.append("Autre")
    names.append("Total")
    values = list(cicounts.values())
    other = 0
    most_common = [label for label, _ in cicounts.most_common(9)]
    for item in cicounts.items():
        if item[0] not in most_common:
            other += item[1]
    values.append(other)
    values.append(sum(cicounts.values()))
    most_common.append("Autre")
    parents = ["Total" if journal in most_common else "" if journal in ["Total"] else "Autre" for journal in names]
    fdata = {
        "names": names,
        "values": values,
        "parents": parents,
    }
    return pd.DataFrame(fdata)


def plot_cities_of_structures_per_article() -> px.sunburst:
    """plots sunburst author per city

    Returns:
        px.sunburst: plotly sunburst plot of city per author
    """
    fdata = pd.read_json(r"data/resources/plots/cities_of_structures_per_article.json")
    fig = px.sunburst(
        fdata,
        names="names",
        parents="parents",
        values="values",
        branchvalues="total",
        color_discrete_sequence=["#612491", "#036287", "#abadff", "#5deee3", "#000091", "#a272ff", "#65c5ae", "#06303a", "#2570f1", "#ffd60c"],
    )
    fig.update_layout(margin=dict(t=0, l=0, r=0, b=0))
    fig.update_traces(textinfo="label+percent entry")
    return fig


def build_publications_per_institution(data: Union[list[dict[str, Any]], dict[Any, Any]], cutoff: int = 25) -> Union[pd.DataFrame, pd.Series]:
    """builds and witres to local file the data for plotting publications_per_institution

    Args:
        data (Union[list[dict[str, Any]], dict[Any, Any]]): the dict containing cleaned institution data
        cutoff (int, optional): a cutoff to show only the first X. Defaults to 25.
    """
    filtered = {item["ror"]: len(item["doi"]) for item in data if isinstance(item["ror"], str)}
    icounts = dict(sorted(filtered.items(), key=lambda kv: kv[1], reverse=True))
    icounts = {"Structures": list(icounts.keys())[:cutoff], "Nombre d'articles SNDS": list(icounts.values())[:cutoff]}
    return icounts


def plot_publications_per_institution() -> px.bar:
    """plots number of publications per Institutions

    Returns:
        px.bar: plotly bar figure number of publications per Institutions
    """
    with open(r"data/resources/plots/publications_per_institution.json", "r", encoding="utf-8") as file_:
        icounts = json.load(file_)
    fig = px.bar(icounts, x="Structures", y="Nombre d'articles SNDS")
    fig.update_traces(marker_color="#036287")
    fig.update_layout(xaxis_tickangle=-60, height=900, plot_bgcolor=BG_COLOR)
    return fig


def plot_publications_per_db_pie(data: Union[pd.DataFrame, pd.Series]) -> None:
    """plots a pie chart of articleDBs

    Args:
        data (Union[pd.DataFrame, pd.Series]): the resources/publications.json
    """
    jcount = Counter(data.articleDB)
    fig = px.pie(
        data,
        values=jcount.values(),
        names=jcount.keys(),
        color_discrete_sequence=["#036287", "#612491"],
    )
    fig.update_layout(showlegend=False)
    fig.update_traces(textinfo="percent+label")
    return fig


def build_map_world_structure_publications(data: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """builds the map world of publications per structure

    Args:
        data (Union[pd.DataFrame, pd.Series]): exploded ror_affiliations.json
    """
    countries = {country.name: country.alpha_3 for country in pycountry.countries}
    stru_count = pd.DataFrame()
    count = Counter(data.country.apply(lambda x: x["country_name"]))
    stru_count["Country"] = count.keys()
    stru_count["Count"] = count.values()
    stru_count["Log Count"] = stru_count["Count"].apply(np.log10)
    stru_count["Codes"] = [countries.get(country, "Unknown code") for country in stru_count["Country"]]
    return pd.DataFrame(stru_count)


def plot_map_world_structure_publications() -> px.choropleth:
    """plots resources/plots/map_world_structure_publications.json

    Returns:
        px.choropleth: the choropleth map
    """
    stru_count = pd.read_json(r"data/resources/plots/map_world_structure_publications.json")
    fig = px.choropleth(
        stru_count,
        locations="Codes",
        color="Log Count",
        hover_name="Country",
        hover_data=["Count"],
        color_continuous_scale=px.colors.sequential.dense,
    )
    fig.update_layout(
        coloraxis_colorbar=dict(
            len=0.75,
            title="Articles",
            x=0.9,
            tickvals=[0, 1, 2, 3, 3.699, 4],
            ticktext=["1", "10", "100", "1000", "5000"],
        ),
        margin=dict(t=10, l=0, r=0, b=10),
    )
    return fig


def build_lang_pie(data: Union[pd.DataFrame, pd.Series]) -> Union[pd.DataFrame, pd.Series]:
    """builds the language pie chart

    Args:
        data (Union[pd.DataFrame, pd.Series]): publications json
    """
    lang = data.explode("Language")
    lang.loc[lang["Language"] == "fre"] = "Français"
    lang.loc[lang["Language"] == "eng"] = "Anglais"
    lang.loc[lang["Language"] == "ger"] = "Allemand"
    lang.loc[lang["Language"] == "ita"] = "Italien"
    lang.loc[lang["Language"] == "gre"] = "Grecque"
    counts = lang.Language.value_counts()
    return counts


def plot_lang_pie() -> px.pie:
    """plots the language pie chart

    Returns:
        px.pie: th pie chart
    """
    count = pd.read_json(r"data/resources/plots/lang_pie.json", typ="series")
    fig = px.pie(
        values=count.values,
        names=count.keys(),
        color_discrete_sequence=["#036287", "#612491"],
    )
    fig.update_layout(showlegend=False)
    fig.update_traces(textinfo="percent+label")
    return fig
