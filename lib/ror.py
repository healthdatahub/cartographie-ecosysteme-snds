import itertools
from json import JSONDecodeError
from typing import Any, Optional, Union

import numpy as np
import pandas as pd
import requests
from tqdm import tqdm

pd.options.mode.chained_assignment = None  # default='warn'


def _find(
    authors: Union[pd.DataFrame, pd.Series],
    institutions: Union[pd.DataFrame, pd.Series],
    affindex: int,
    item: list,
) -> Optional[tuple[int, str]]:
    """finds author aff labels in institutions

    Args:
        authors (Union[pd.DataFrame, pd.Series]): unique_authors.json
        institutions (Union[pd.DataFrame, pd.Series]): cleaned_affiliations.json
        affindex (int): index of the aff in cleaned_affiliations.json
        item (list): _description_

    Returns:
        Optional[tuple[int, str]]: index in cleaned_affiliations.json and label
    """
    for index, affs in enumerate(authors.AffiliationInfo):
        for art in affs:
            if isinstance(art, list):
                for aff in art:
                    if aff == item:
                        return index, institutions.iloc[affindex]["ror"]


def auth_inst_correspondence(
    authors: Union[pd.DataFrame, pd.Series],
    institutions: Union[pd.DataFrame, pd.Series],
):
    """goes through all affs and adds them to authors

    Args:
        authors (Union[pd.DataFrame, pd.Series]): unique_authors.json
        institutions (Union[pd.DataFrame, pd.Series]): cleaned_affiliations.json
    """
    authors["Institutions/label"] = np.empty((len(authors), 0)).tolist()
    for index, i in enumerate(institutions["raw"]):
        for j in i:
            res = _find(authors, institutions, index, j)
            if res:
                matches = [match for match in res]
            try:
                authors["Institutions/label"][matches[0]].append(matches[1])
            except IndexError:
                continue
    authors["Institutions/label"] = authors["Institutions/label"].apply(lambda x: list(dict.fromkeys(x)))
    return authors


def ror_search(aff: str, staging: bool = False) -> dict:
    """function does a simple affiliation search on https://ror.org/

    Args:
        aff (str): an affiliation
        staging (bool): Defaults to False. If True uses ROR staging API
    Returns:
        dict: the output from the search or an empty dict if an error occurs
    """
    url_structures = f"https://api.ror.org/organizations?affiliation={aff}"
    if staging:
        url_structures = f"https://api.staging.ror.org/organizations?affiliation={aff}"
    ror_out = requests.get(url_structures)
    try:
        return ror_out.json()
    except JSONDecodeError:
        return {}


def ror_query(aff: str) -> dict:
    """function does a simple affiliation search on https://ror.org/

    Args:
        aff (str): an affiliation

    Returns:
        dict: the output from the search or an empty dict if an error occurs
    """
    url_structures = f"https://api.ror.org/organizations?query={aff}"
    ror_out = requests.get(url_structures)
    try:
        return ror_out.json()
    except JSONDecodeError:
        return {}


def _get_chosen(result: dict) -> list[dict]:
    """function gets the chosen ror orgs that matched the affilitation search

    Args:
        result (dict): the result from ror_search(*args)

    Returns:
        list[dict]: a list of chosen orgs from ror_search(*args)
    """
    chosen = []
    for item in result["items"]:
        if item["chosen"]:
            chosen.append(item)
    return chosen


def _get_org_info(elements: pd.Series) -> dict[str, Any]:
    """function extracts the name from the chosen organiations

    Args:
        elements (pd.Series): all the chosen outputs from the ror_search(*args) + _get_chosen(*args)

    Returns:
        dict[str, Any]: a dict | org name - org info
    """
    orgs = {}
    for org in elements:
        name = org["organization"]["name"]
        orgs[name] = org["organization"]
    return orgs


class ROR:
    """the class for all methods related to aff/ror search"""

    def __init__(self, exploded_authors, path, force=False) -> None:
        self.path = path
        self.prev_file = self.path / "affiliations/prev_all_affiliations.json"
        self.previous_results = pd.read_json(self.prev_file)
        self.affs = self.create_affiliation_df(exploded_authors)
        self.ror_aff_db: dict[str, list[str]] = {}
        self.ror_info_db: dict[str, Any] = {}
        self.ror_df = pd.DataFrame()
        self.force = force

    def create_affiliation_df(self, exploded_authors: pd.DataFrame) -> pd.DataFrame:
        """uses exploded_authors and groupby affiliations

        Args:
            exploded_authors (pd.DataFrame): the exploded_authors.csv dataframe

        Returns:
            pd.DataFrame: the exploded_authors.csv dataframe with new columns for affiliations
        """
        data = exploded_authors.explode("AffiliationInfo")
        data.AffiliationInfo = data.AffiliationInfo.apply(lambda x: None if isinstance(x, dict) else x)
        data.AffiliationInfo = data.AffiliationInfo.apply(lambda x: None if isinstance(x, list) else x)
        data = (
            data.groupby("AffiliationInfo")
            .agg(
                {
                    "PMID": set,
                    "doi": set,
                    "NormalizedName": set,
                    "FullName": set,
                }
            )
            .reset_index()
        )
        data["ror_searched"] = False
        data["ror_chosen"] = [{} for _ in range(len(data))]
        return data

    def convert_previous_results(self):
        """converts previours ror results to new"""
        self.previous_results = self.previous_results.rename(
            {
                "Affiliations": "AffiliationInfo",
                "Normalized Name": "NormalizedName",
                "All Names": "FullName",
            },
            axis="columns",
        )
        for col in ["Name", "PmcRefCount"]:
            try:
                self.previous_results = self.previous_results.drop(columns=[col])
            except KeyError:
                pass
        self.previous_results.PMID = self.previous_results.PMID.apply(set)
        self.previous_results.doi = self.previous_results.doi.apply(set)
        self.previous_results.NormalizedName = self.previous_results.NormalizedName.apply(set)
        self.previous_results.FullName = self.previous_results.FullName.apply(set)
        self.previous_results.to_json(self.prev_file, indent=2)

    def get_previous_ror_results(self):
        """function gets the previous google searches and inserts them in current dataframe"""
        new_affs = self.affs[np.isin(self.affs["AffiliationInfo"], self.previous_results["AffiliationInfo"], invert=True)]
        self.affs = pd.concat([new_affs, self.previous_results], ignore_index=True)

    def do_full_ror_search(self):
        """method does the ror search for all affs in self.affs"""
        if self.force:
            total = len(self.affs)
        else:
            try:
                total = self.affs.ror_searched.value_counts()[False]
            except KeyError:
                total = 0
        for index, aff in tqdm(
            enumerate(self.affs.AffiliationInfo),
            desc="ROR.org API search",
            total=total,
        ):
            if not self.affs.ror_searched[index] or self.force:
                result = ror_search(aff)
                if result:
                    self.affs.ror_searched[index] = True
                    self.affs.ror_chosen[index] = _get_chosen(result)
                    self.affs.to_json(self.path / "affiliations/all_affiliations.json", indent=2)

    def _organize_org_info(self):
        """extracts infos from the search results and reorganizes them for DataFrame usage"""
        for index in self.affs.index:
            ror_orgs = _get_org_info(self.affs.ror_chosen[index])
            raw = self.affs.AffiliationInfo[index]
            for key, item in ror_orgs.items():
                if key in self.ror_aff_db:
                    self.ror_aff_db[key].append(raw)
                else:
                    self.ror_aff_db[key] = [raw]
                    self.ror_info_db[key] = item

    def ror_to_df(self):
        """puts all ror info in a DataFrame"""
        self._organize_org_info()
        self.ror_df["ror"] = self.ror_aff_db.keys()
        self.ror_df["raw"] = self.ror_aff_db.values()
        info_db = {}
        for key in list(self.ror_info_db.values())[0].keys():
            info_db[key] = []
            for info in self.ror_info_db.values():
                info_db[key].append(info[key])
            self.ror_df[key] = info_db[key]

    def add_article_info(self):
        """method adds new columns from articles to self.ror_df"""
        ror_df_exploded = self.ror_df.explode("raw")
        merged = pd.merge(ror_df_exploded.set_index("raw"), self.affs.set_index("AffiliationInfo"), left_index=True, right_index=True)
        aff_info = merged.groupby(by="ror").agg(
            {
                "PMID": lambda x: set(itertools.chain.from_iterable(x)),
                "doi": lambda x: set(itertools.chain.from_iterable(x)),
                "NormalizedName": lambda x: set(itertools.chain.from_iterable(x)),
                "FullName": lambda x: set(itertools.chain.from_iterable(x)),
            }
        )
        self.ror_df = pd.merge(aff_info, self.ror_df.set_index("ror"), left_index=True, right_index=True).reset_index()
        self.ror_df = self.ror_df.drop(columns="name")


def main() -> None:
    """main func"""
    rorsearch = ROR()
    rorsearch.get_previous_ror_results()
    rorsearch.do_full_ror_search()
    rorsearch.ror_to_df()
    rorsearch.add_article_info()
    rorsearch.ror_df.to_json(r"data/resources/ror_affiliations.json", indent=2)
    rorsearch.affs.to_json(rorsearch.prev_file, indent=2)


if __name__ == "__main__":
    main()
