"""script gets new articles and rewrites databases for the app"""
import datetime

from lib import build, query


def main():
    """queries PubMes, LiSSa, HAL & builds data"""

    query.main()
    build.Builder()
    return datetime.datetime.now()


if __name__ == "__main__":
    main()
